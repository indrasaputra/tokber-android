package com.tb.tokobersama;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.tb.tokobersama.model.Category;
import com.tb.tokobersama.model.ResponseCategory;
import com.tb.tokobersama.model.ResponseUpload;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.ImagePicker;
import com.tb.tokobersama.util.RestAdapter;
import com.tb.tokobersama.util.Tools;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.R.layout.simple_spinner_item;

public class AddProductActivity extends BaseActivity {

    private Spinner sp_category;
    List<Category> categories = new ArrayList<>();
    private ArrayList<String> categoryList = new ArrayList<>();
    private EditText et_product_name;
    private EditText et_product_desc;
    private EditText et_price;
    private EditText et_stock;
    private ImageView image_1, image_2, image_3, image_4;

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1891;
    private static final int PICK_IMAGE_ID = 2345;
    private String mCurrentPhotoPath, fileFoto;
    private String fileFoto1="", fileFoto2="", fileFoto3="", fileFoto4="";
    private final int PICTURE_1 = 1;
    private final int PICTURE_2 = 2;
    private final int PICTURE_3 = 3;
    private final int PICTURE_4 = 4;
    private int PICTURE_ID = 0;
    private Map<String, RequestBody> textMap;
    private List<MultipartBody.Part> fileList;
    private Thread getThread;
    private boolean fromHome;

    private AddProductActivity myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        myApp = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString("Upload Produk");
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        Intent intent = getIntent();
        fromHome = intent.getBooleanExtra("fromHome",false);

        initComponent();
    }

    private void initComponent() {
        loadUserProfile();

        Tools.checkAndRequestPermissions(myApp);

        et_product_name = findViewById(R.id.et_product_name);
        et_product_desc = findViewById(R.id.et_product_desc);
        et_price = findViewById(R.id.et_price);
        et_stock = findViewById(R.id.et_stock);
        image_1 = findViewById(R.id.image_1);
        image_2 = findViewById(R.id.image_2);
        image_3 = findViewById(R.id.image_3);
        image_4 = findViewById(R.id.image_4);

        sp_category = findViewById(R.id.sp_category);
        requestCategory();

        final Button btn_upload = findViewById(R.id.btn_upload);
        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(btn_upload.getWindowToken(), 0);
                uploadProduk();
            }
        });

        image_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(image_1.getWindowToken(), 0);
                PICTURE_ID = PICTURE_1;
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(myApp);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
            }
        });
        image_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(image_2.getWindowToken(), 0);
                PICTURE_ID = PICTURE_2;
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(myApp);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
            }
        });
        image_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(image_3.getWindowToken(), 0);
                PICTURE_ID = PICTURE_3;
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(myApp);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
            }
        });
        image_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(image_4.getWindowToken(), 0);
                PICTURE_ID = PICTURE_4;
                Intent chooseImageIntent = ImagePicker.getPickImageIntent(myApp);
                startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
            }
        });

        sp_category.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(sp_category.getWindowToken(), 0);
                return false;
            }
        });
    }

    private File createImageFile() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("HHmmss").format(new Date());
        //String imageFileName = "tb_" + timeStamp + "_";
        String imageFileName = "tb_";
        File storageDir = null;
        if(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) != null) {
            storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        } else if(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) != null){
            storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        } else if(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) != null){
            storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        } else {
            storageDir = this.getCacheDir();
        }
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  // prefix
                    ".jpg",   // suffix
                    storageDir      // directory
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if (requestCode==PICK_IMAGE_ID) {
            if (resultCode == Activity.RESULT_OK) {
                final Bitmap mBitmap = ImagePicker.getImageFromResult(myApp, resultCode, data);
                //image_1.setImageBitmap(mBitmap);
                System.out.println("absolute path " + mCurrentPhotoPath);
                //File file = null;
                File resizedFile = createImageFile(); //new File(myFragment.getContext().getCacheDir(), "foto.png");
                BufferedOutputStream bos = null;
                InputStream bis = null;
                try {
                    bos = new BufferedOutputStream(new FileOutputStream(resizedFile), 1024);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    bis = new ByteArrayInputStream(stream.toByteArray());

                    byte[] buffer = new byte[1024];

                    int len = 0;

                    while ((len = bis.read(buffer)) != -1) {
                        bos.write(buffer, 0, len);
                    }
                    //Uri selectedImage = Uri.fromFile(resizedFile);
                    Uri selectedImage = FileProvider.getUriForFile(AddProductActivity.this, BuildConfig.APPLICATION_ID + ".provider",resizedFile);
                    //fileFoto = GetPathFromUri.getPath(myFragment.getContext(), selectedImage);
                    fileFoto = resizedFile.getAbsolutePath();
                    switch (PICTURE_ID) {
                        case PICTURE_1:
                            fileFoto1 = fileFoto;
                            image_1.setImageBitmap(mBitmap);
                            image_2.setVisibility(View.VISIBLE);
                            break;
                        case PICTURE_2:
                            fileFoto2 = fileFoto;
                            image_2.setImageBitmap(mBitmap);
                            image_3.setVisibility(View.VISIBLE);
                            break;
                        case PICTURE_3:
                            fileFoto3 = fileFoto;
                            image_3.setImageBitmap(mBitmap);
                            image_4.setVisibility(View.VISIBLE);
                            break;
                        case PICTURE_4:
                            image_4.setImageBitmap(mBitmap);
                            fileFoto4 = fileFoto;
                            break;
                    }
                    System.out.println("resize path : " + fileFoto);
                    System.out.println("resize path : " + resizedFile.getAbsolutePath());
//                    if(fileFoto == null){
//                        fileFoto = resizedFile.getAbsolutePath();
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        bis.close();
                    } catch (Exception ignored) {
                    }
                    try {
                        bos.close();
                    } catch (Exception ignored) {
                    }
                }
            }
        }
    }

    private void requestCategory() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("session", userProfile.getSession());

        DataService api = RestAdapter.createAPI();

        Call<ResponseCategory> call = api.getCategoryList(parameters);

        call.enqueue(new Callback<ResponseCategory>() {
            @Override
            public void onResponse(Call<ResponseCategory> call, Response<ResponseCategory> response) {
                ResponseCategory resp = response.body();
                if (resp!=null && resp.RC.equals("00")) {
                    categories = resp.categories;
                    categoryList = new ArrayList<>();
                    for (int i=0; i<categories.size();i++) {
                        categoryList.add(categories.get(i).name);
                    }
                    setSp_category();
                }
            }

            @Override
            public void onFailure(Call<ResponseCategory> call, Throwable t) {

            }
        });
    }

    private void setSp_category() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, simple_spinner_item, categoryList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_category.setAdapter(spinnerArrayAdapter);
    }

    private void uploadProduk() {
        View focusView = null;
        boolean cancel = false;

        if (TextUtils.isEmpty(et_product_name.getText())) {
            et_product_name.setError(getResources().getString(R.string.notif_mandatory));
            focusView = et_product_name;
            cancel = true;
        }

        if (TextUtils.isEmpty(et_product_desc.getText())) {
            et_product_desc.setError(getResources().getString(R.string.notif_mandatory));
            focusView = et_product_desc;
            cancel = true;
        }

        if (TextUtils.isEmpty(et_price.getText())) {
            et_price.setError(getResources().getString(R.string.notif_mandatory));
            focusView = et_price;
            cancel = true;
        }

        if (TextUtils.isEmpty(et_stock.getText())) {
            et_stock.setError(getResources().getString(R.string.notif_mandatory));
            focusView = et_stock;
            cancel = true;
        }

        RequestBody merchant_id = RequestBody.create(MediaType.parse("text/plain"),userProfile.getId());
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"),et_product_name.getText().toString());
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"),et_product_desc.getText().toString());
        RequestBody stock = RequestBody.create(MediaType.parse("text/plain"),et_stock.getText().toString());
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"),et_price.getText().toString());
        RequestBody category_id = RequestBody.create(MediaType.parse("text/plain"),categories.get(sp_category.getSelectedItemPosition()).id);
        RequestBody reg_by = RequestBody.create(MediaType.parse("text/plain"),"Android");
        RequestBody newtag = RequestBody.create(MediaType.parse("text/plain"),"1");
        RequestBody session = RequestBody.create(MediaType.parse("text/plain"),userProfile.getSession());
        RequestBody trxid = RequestBody.create(MediaType.parse("text/plain"),Tools.getTrxid(userProfile.getId()));

        textMap = new HashMap<>();
        textMap.put("merchant_id", merchant_id);
        textMap.put("name",name);
        textMap.put("description",description);
        textMap.put("stock",stock);
        textMap.put("price",price);
        textMap.put("category_id",category_id);
        textMap.put("reg_by",reg_by);
        textMap.put("new",newtag);
        textMap.put("session",session);
        textMap.put("trxid",trxid);

        fileList = new ArrayList<>();
        if (!fileFoto1.equals("")) {
            File file = new File(fileFoto1);
            RequestBody picture = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part pic_body = MultipartBody.Part.createFormData("picture_1", file.getName(), picture);
            fileList.add(pic_body);
        }
        if (!fileFoto2.equals("")) {
            File file = new File(fileFoto2);
            RequestBody picture = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part pic_body = MultipartBody.Part.createFormData("picture_2", file.getName(), picture);
            fileList.add(pic_body);
        }
        if (!fileFoto3.equals("")) {
            File file = new File(fileFoto3);
            RequestBody picture = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part pic_body = MultipartBody.Part.createFormData("picture_3", file.getName(), picture);
            fileList.add(pic_body);
        }
        if (!fileFoto4.equals("")) {
            File file = new File(fileFoto4);
            RequestBody picture = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part pic_body = MultipartBody.Part.createFormData("picture_4", file.getName(), picture);
            fileList.add(pic_body);
        }

        if (fileList.size()==0) {
            cancel = true;
            Toast.makeText(myApp,"Mohon unggah foto produk", Toast.LENGTH_LONG).show();
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            //callUploadApi();
            showPinDialog();
        }
    }

    private void showPinDialog() {
        final AlertDialog pinAuth = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = LayoutInflater.from(this);
        final View dialogView = inflater.inflate(R.layout.layout_pin_dialog, null);
        Log.e("fromHome",fromHome+"");
        //pinAuth.setTitle("PIN Required.");
        pinAuth.setCancelable(true);
        pinAuth.setView(dialogView);

        final EditText pin = (EditText) dialogView.findViewById(R.id.et_pin);
        //pin.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        final Button btn_ok = dialogView.findViewById(R.id.btn_ok);

        pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                boolean isReady = pin.getText().toString().length() >= 6;
                btn_ok.setEnabled(isReady);
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("PIN", pin.getText().toString()+" vs "+userProfile.getPin());
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(dialogView.getWindowToken(), 0);
                if (sha256(pin.getText().toString()).equals(userProfile.getPin())) {
                    callUploadApi();
                    pinAuth.dismiss();
                } else {
                    Toast.makeText(myApp,"PIN salah", Toast.LENGTH_LONG).show();
                }
            }
        });

        pinAuth.show();
    }

    private void callUploadApi() {
        final ProgressBar pb = findViewById(R.id.progress_circular);
        pb.setVisibility(View.VISIBLE);

        getThread = new Thread(new Runnable() {
            @Override
            public void run() {
                DataService api = RestAdapter.createAPI();

                Call<ResponseUpload> call = api.uploadProduct(textMap,fileList);

                call.enqueue(new Callback<ResponseUpload>() {
                    @Override
                    public void onResponse(Call<ResponseUpload> call, Response<ResponseUpload> response) {
                        ResponseUpload resp = response.body();
                        if (resp!=null && resp.rc.equals("00")) {
                            Toast.makeText(myApp,resp.description, Toast.LENGTH_LONG).show();
                            int TIME_OUT = 2000;
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    pb.setVisibility(View.INVISIBLE);
                                    Intent returnIntent = new Intent();
                                    //returnIntent.putExtra("result", result);
                                    setResult(Activity.RESULT_OK,returnIntent);
                                    finish();
                                }
                            }, TIME_OUT);
                        } else {
                            pb.setVisibility(View.INVISIBLE);
                            Toast.makeText(myApp,resp.description, Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseUpload> call, Throwable t) {
                        pb.setVisibility(View.INVISIBLE);
                        Toast.makeText(myApp,"Connection Error", Toast.LENGTH_LONG).show();
                        getThread.interrupt();
                    }
                });
            }
        });
        getThread.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}