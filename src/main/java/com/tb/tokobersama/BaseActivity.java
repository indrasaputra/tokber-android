package com.tb.tokobersama;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.tb.tokobersama.model.ResponseToken;
import com.tb.tokobersama.model.UserProfile;
import com.tb.tokobersama.util.Constants;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;
import com.tb.tokobersama.util.Tools;

import java.security.MessageDigest;
import java.util.HashMap;

public class BaseActivity extends AppCompatActivity {

    public String MY_PREFS_NAME = "toko_bersama";
    public String API_BASE_URL = Constants.API_BASE_URL;
    public String key_string = Constants.key_string;
    public Thread getThread = null;
    public SharedPreferences prefs;
    public SharedPreferences.Editor editor;
    public UserProfile userProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_base);

        prefs = getSharedPreferences(MY_PREFS_NAME, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public boolean checkingSignIn() {
        //getting shared preference
        //SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        Log.e("user", "" + prefs.getString("user_phone", null));
        // check user is created or not
        // if user is already logged in
        if (prefs.getString("user_phone", null) != null) {
            String userid = prefs.getString("user_phone", null);
            return !userid.equals("delete");
        } else {
            return false;
        }
    }

    public void logout() {
        editor.putString("user_phone","delete");
        editor.commit();
    }

    public void loadUserProfile() {
        //if (checkingSignIn()) {
            userProfile = Tools.getUserProfile(this); //new UserProfile();

            /*userProfile.setId(prefs.getString("user_id",null));
            userProfile.setPhone(prefs.getString("user_phone",null));
            userProfile.setName(prefs.getString("user_name",null));
            userProfile.setAddress(prefs.getString("user_address",null));
            userProfile.setProvince(prefs.getString("user_province",null));
            userProfile.setCity(prefs.getString("user_city",null));//user_city,
            userProfile.setEmail(prefs.getString("user_email",null));//user_email,
            userProfile.setStore_name(prefs.getString("user_store_name",null));//user_store_name,
            userProfile.setStore_description(prefs.getString("user_store_description",null));//user_store_description,
            userProfile.setBalance(prefs.getString("user_balance",null));//user_balance,
            userProfile.setReg_date(prefs.getString("user_reg_date",null));//user_reg_date,
            userProfile.setUpline(prefs.getString("user_upline",null));//user_upline,
            userProfile.setLon(prefs.getString("user_lon",null));//user_lon,
            userProfile.setLat(prefs.getString("user_lat",null));//user_lat,
            userProfile.setBank_name(prefs.getString("user_bank_name",null));//user_bank_name,
            userProfile.setAccount_name(prefs.getString("user_bank_account_name",null));//user_bank_account_name,
            userProfile.setBank_account_number(prefs.getString("user_bank_account_number",null));//user_bank_account_number,
            userProfile.setPostal_code(prefs.getString("user_postal_code",null));//user_postal_code,
            userProfile.setStatus(prefs.getString("user_status",null));//user_status,
            userProfile.setSession(prefs.getString("user_session",null));//user_session;*/
        //}
    }

    public void initializeFirebase(final String topic, final boolean uploadToken) {
        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.d("Firebase Token",newToken);
                if (uploadToken) {
                    uploadToken(newToken);
                }
                //santri.firebasechat.managers.Utils.uploadToken(newToken);
            }
        });

        FirebaseMessaging.getInstance().subscribeToTopic(topic)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Log.d("Subscribe to Topic", topic);
                        //Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void uploadToken(String token) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("phone", userProfile.getPhone());
        parameters.put("token", token);
        parameters.put("session", userProfile.getSession());

        DataService api = RestAdapter.createAPI();

        Call<ResponseToken> call = api.uploadToken(parameters);

        call.enqueue(new Callback<ResponseToken>() {
            @Override
            public void onResponse(Call<ResponseToken> call, Response<ResponseToken> response) {
                ResponseToken resp = response.body();
                if (resp!=null && resp.RC.equals("00")) {
                    Log.d("upload Token Result", resp.description);
                } else {
                    Log.e("upload Token Result", resp.RC + " " + resp.description);
                }
            }

            @Override
            public void onFailure(Call<ResponseToken> call, Throwable t) {
                Log.e("upload Token Error", t.getMessage());
            }
        });
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public boolean isEmailValid(String email) {
        //TODO change for your own logic
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public boolean isPhoneValid(String phone) {
        //TODO change for your own logic
        String ePattern = "^[+]?[0-9]{10,13}$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(phone);
        return m.matches();
    }

    public boolean isPasswordValid(String password) {
        //TODO change for your own logic
        return password.length() > 4;
    }

    public String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(("tokber"+base).getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            Log.d("sha256 result",hexString.toString());
            return hexString.toString();
        } catch(Exception ex){
            throw new RuntimeException(ex);
        }
    }
}