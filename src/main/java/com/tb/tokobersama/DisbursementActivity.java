package com.tb.tokobersama;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tb.tokobersama.model.Merchant;
import com.tb.tokobersama.model.ResponseMerchant;
import com.tb.tokobersama.model.ResponseToken;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;
import com.tb.tokobersama.util.Tools;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Locale;

public class DisbursementActivity extends BaseActivity {

    private Spinner sp_bank;
    private TextView text_balance;
    private EditText et_amount, et_account, et_acc_name;
    private DisbursementActivity myApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disbursement);

        myApp = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString("Pencairan Saldo");
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        initComponent();
    }

    private void initComponent() {
        loadUserProfile();

        sp_bank = findViewById(R.id.sp_bank);
        text_balance = findViewById(R.id.text_balance);
        et_amount = findViewById(R.id.et_amount);
        et_account = findViewById(R.id.et_account);
        et_acc_name = findViewById(R.id.et_acc_name);

        requestMerchantInfo();

        et_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String input = charSequence.toString();

                if (!input.isEmpty()) {

                    input = input.replace(".", "");

                    String newPrice = String.format(Locale.ITALY,"%1$,.0f", Float.parseFloat(input));

                    et_amount.removeTextChangedListener(this); //To Prevent from Infinite Loop

                    et_amount.setText(newPrice);
                    et_amount.setSelection(newPrice.length()); //Move Cursor to end of String

                    et_amount.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //et_amount.setText(String.format(Locale.ITALY,"%1$,.0f", Float.parseFloat(et_amount.getText().toString())));
            }
        });

        final Button btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitDisbursement();
            }
        });

        sp_bank.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(sp_bank.getWindowToken(), 0);
                return false;
            }
        });
    }

    private void submitDisbursement() {
        View focusView = null;
        boolean cancel = false;

        if (TextUtils.isEmpty(et_amount.getText())) {
            et_amount.setError(getResources().getString(R.string.notif_mandatory));
            focusView = et_amount;
            cancel = true;
        }
        if (TextUtils.isEmpty(et_account.getText())) {
            et_account.setError(getResources().getString(R.string.notif_mandatory));
            focusView = et_account;
            cancel = true;
        }
        if (TextUtils.isEmpty(et_acc_name.getText())) {
            et_acc_name.setError(getResources().getString(R.string.notif_mandatory));
            focusView = et_acc_name;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showPinDialog();
        }
    }

    private void showPinDialog() {
        final AlertDialog pinAuth = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = LayoutInflater.from(this);
        final View dialogView = inflater.inflate(R.layout.layout_disburse_pin_dialog, null);
        pinAuth.setCancelable(true);
        pinAuth.setView(dialogView);

        TextView text_bank_name = dialogView.findViewById(R.id.text_bank_name);
        TextView text_bank_acc = dialogView.findViewById(R.id.text_bank_acc);
        TextView text_nominal = dialogView.findViewById(R.id.text_nominal);
        TextView text_acc_name = dialogView.findViewById(R.id.text_acc_name);

        text_bank_name.setText(sp_bank.getSelectedItem().toString());
        text_bank_acc.setText(et_account.getText().toString());
        text_acc_name.setText(et_acc_name.getText().toString());
        text_nominal.setText("Rp " + et_amount.getText().toString());

        final EditText pin = (EditText) dialogView.findViewById(R.id.et_pin);
        //pin.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        final Button btn_ok = dialogView.findViewById(R.id.btn_ok);

        pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                boolean isReady = pin.getText().toString().length() >= 6;
                btn_ok.setEnabled(isReady);
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("PIN", pin.getText().toString()+" vs "+userProfile.getPin());
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(dialogView.getWindowToken(), 0);
                if (sha256(pin.getText().toString()).equals(userProfile.getPin())) {
                    sendDisbursement();
                    pinAuth.dismiss();
                } else {
                    Toast.makeText(myApp,"PIN salah", Toast.LENGTH_LONG).show();
                }
            }
        });

        pinAuth.show();
    }

    private void sendDisbursement() {
        final ProgressBar pb = findViewById(R.id.progress_circular);
        pb.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        getThread = new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("merchant_id",userProfile.getId());
                parameters.put("session",userProfile.getSession());
                parameters.put("trxid", Tools.getTrxid(userProfile.getId()));
                parameters.put("bank",sp_bank.getSelectedItem().toString());
                parameters.put("accno",et_account.getText().toString());
                parameters.put("amount",et_amount.getText().toString().replace(".",""));
                parameters.put("accname",et_acc_name.getText().toString());
                parameters.put("pin",userProfile.getPin());

                DataService api = RestAdapter.createAPI();

                Call<ResponseToken> call = api.requestDisbursement(parameters);

                call.enqueue(new Callback<ResponseToken>() {
                    @Override
                    public void onResponse(Call<ResponseToken> call, Response<ResponseToken> response) {
                        ResponseToken resp = response.body();
                        if (resp!=null) {
                            if (resp.rc.equals("00")) {
                                showPromptDialog(true, "");
                            } else {
                                showPromptDialog(false, resp.description);
                            }
                        } else {
                            Toast.makeText(myApp,"Server Error", Toast.LENGTH_LONG).show();
                            getThread.interrupt();
                        }
                        pb.setVisibility(View.INVISIBLE);
                        getThread.interrupt();
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }

                    @Override
                    public void onFailure(Call<ResponseToken> call, Throwable t) {
                        pb.setVisibility(View.INVISIBLE);
                        Toast.makeText(myApp,"Connection Error", Toast.LENGTH_LONG).show();
                        getThread.interrupt();
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }
                });
            }
        });
        getThread.start();
    }

    private void showPromptDialog(boolean successful, String message) {
        final boolean success = successful;
        final AlertDialog prompt = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = LayoutInflater.from(this);
        final View dialogView = inflater.inflate(R.layout.layout_disburse_dialog, null);
        prompt.setCancelable(false);
        prompt.setView(dialogView);
        prompt.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView text_bank_name = dialogView.findViewById(R.id.text_bank_name);
        TextView text_bank_acc = dialogView.findViewById(R.id.text_bank_acc);
        TextView text_nominal = dialogView.findViewById(R.id.text_nominal);
        TextView text_acc_name = dialogView.findViewById(R.id.text_acc_name);
        TextView text_result = dialogView.findViewById(R.id.text_result);
        TextView text_pesan = dialogView.findViewById(R.id.text_pesan);
        ImageView img_status = dialogView.findViewById(R.id.img_status);

        text_bank_name.setText(sp_bank.getSelectedItem().toString());
        text_bank_acc.setText(et_account.getText().toString());
        text_acc_name.setText(et_acc_name.getText().toString());
        text_nominal.setText("Rp " + et_amount.getText().toString());
        text_pesan.setText(message);

        if (success) {
            text_result.setText("BERHASIL");
            text_pesan.setVisibility(View.GONE);
            img_status.setImageDrawable(getResources().getDrawable(R.drawable.icon_success));
        } else {
            text_result.setText("GAGAL");
            img_status.setImageDrawable(getResources().getDrawable(R.drawable.icon_fail));
        }

        Button btn_ok = dialogView.findViewById(R.id.btn_done);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (success) {
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                } else {
                    prompt.dismiss();
                }
            }
        });

        prompt.show();
    }

    private void requestMerchantInfo() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("id",userProfile.getId());
        parameters.put("session",userProfile.getSession());

        DataService api = RestAdapter.createAPI();

        Call<ResponseMerchant> call = api.getMerchantInfo(parameters);

        call.enqueue(new Callback<ResponseMerchant>() {
            @Override
            public void onResponse(Call<ResponseMerchant> call, Response<ResponseMerchant> response) {
                ResponseMerchant resp = response.body();
                if (resp!=null && resp.RC.equals("00")) {
                    Merchant merchant = resp.merchants.get(0);
                    String saldo = null;
                    if(merchant.balance==null)
                        saldo = "0";
                    else
                        saldo = merchant.balance;
                    text_balance.setText(String.format(Locale.ITALY,"%1$,.0f", Float.parseFloat(saldo)));

                } else {
                    text_balance.setText("-");
                }
            }

            @Override
            public void onFailure(Call<ResponseMerchant> call, Throwable t) {
                if (!call.isCanceled()) {
                    text_balance.setText("-");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}