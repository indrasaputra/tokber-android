package com.tb.tokobersama;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tb.tokobersama.util.Constants;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.UUID;

public class LoginActivity extends BaseActivity {

    private EditText edit_user, edit_password;
    private Button btn_login;
    private LoginActivity myApp;
    private ProgressBar progressBar;
    private String deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        myApp = this;

        if (checkingSignIn()) {
            //launchHomeScreen();
        }

        edit_user = findViewById(R.id.edit_user);
        edit_password = findViewById(R.id.edit_password);
        btn_login = findViewById(R.id.btn_login);
        progressBar = findViewById(R.id.progress_circular);

        if (prefs.getString("login_phone",null)!=null)
            edit_user.setText(prefs.getString("login_phone",null));

        //getDeviceId();

        TextView txt_create = findViewById(R.id.txt_create);

        progressBar.setVisibility(View.INVISIBLE);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateInput()) {
                    getLogin();
                }
            }
        });

        txt_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });

        initializeFirebase("all",false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        Log.e("Activity Result",requestCode+" "+resultCode);
        if (requestCode==202) {
            if (resultCode== Activity.RESULT_OK) {
                getLogin();
            }
        }
    }

    public String getDeviceId() {
        deviceId = "";

        try {
            FileInputStream fileIn = openFileInput("id.device");
            InputStreamReader InputRead= new InputStreamReader(fileIn);

            char[] inputBuffer= new char[100];
            String s="";
            int charRead;

            while ((charRead=InputRead.read(inputBuffer))>0) {
                // char to string conversion
                String readstring=String.copyValueOf(inputBuffer,0,charRead);
                s +=readstring;
            }
            InputRead.close();
            deviceId=s;
        } catch (Exception e) {
            Log.e("Error opening file",e.getMessage());
            try {
                FileOutputStream fileout = openFileOutput("id.device", MODE_PRIVATE);
                OutputStreamWriter outputWriter = new OutputStreamWriter(fileout);
                deviceId = UUID.randomUUID().toString();
                outputWriter.write(deviceId);
                outputWriter.close();
            } catch (IOException ex) {
                Log.e("Error writing file", ex.getMessage());
            }
        }

        Log.d("device ID =====", deviceId);

        return deviceId;
    }

    private boolean validateInput() {
        boolean valid = true;

        if (TextUtils.isEmpty(edit_user.getText())) {
            edit_user.setError("Wajib diisi");
            valid = false;
            edit_user.requestFocus();
        }

        if (TextUtils.isEmpty(edit_password.getText())) {
            edit_password.setError("Password harus diisi");
            valid = false;
            edit_password.requestFocus();
        }

//        if (isPhoneValid(edit_user.getText().toString())) {
//            edit_user.setError("No HP tidak valid");
//            valid = false;
//            edit_user.requestFocus();
//        }

        return valid;
    }

    private void getLogin() {
        progressBar.setVisibility(View.VISIBLE);
        getThread = new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("phone",edit_user.getText().toString());
                parameters.put("passwd",sha256(edit_password.getText().toString()));
                parameters.put("deviceid",getDeviceId());

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.API_BASE_URL)
                        .addConverterFactory(ScalarsConverterFactory.create())
                        //.client(Utility.getHeader(httpBody))
                        .build();

                DataService api = retrofit.create(DataService.class);

                Call<String> call = api.login(parameters);

                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        Log.e("Responsestring", response.toString());

                        if (response.isSuccessful()) {
                            if (response.body()!=null) {
                                Log.d("response body", response.body().toString());
                                try {
                                    JSONObject jsonObj = new JSONObject(response.body().toString());
                                    String RC = jsonObj.getString("RC");

                                    if (RC.equals("00")) {
                                        editor.putString("user_id",jsonObj.getString("id"));
                                        editor.putString("user_phone",jsonObj.getString("phone"));
                                        editor.putString("user_name",jsonObj.getString("name"));
                                        editor.putString("user_address",jsonObj.getString("address"));
                                        editor.putString("user_province",jsonObj.getString("province"));
                                        editor.putString("user_city",jsonObj.getString("city"));
                                        editor.putString("user_email",jsonObj.getString("email"));
                                        editor.putString("user_store_name",jsonObj.getString("store_name"));
                                        editor.putString("user_store_description",jsonObj.getString("store_description"));
                                        editor.putString("user_balance",jsonObj.getString("balance"));
                                        editor.putString("user_reg_date",jsonObj.getString("reg_date"));
                                        editor.putString("user_upline",jsonObj.getString("upline"));
                                        editor.putString("user_lon",jsonObj.getString("lon"));
                                        editor.putString("user_lat",jsonObj.getString("lat"));
                                        editor.putString("user_bank_name",jsonObj.getString("bank_name"));
                                        editor.putString("user_bank_account_name",jsonObj.getString("bank_account_name"));
                                        editor.putString("user_bank_account_number",jsonObj.getString("bank_account_number"));
                                        editor.putString("user_postal_code",jsonObj.getString("postal_code"));
                                        editor.putString("user_status",jsonObj.getString("status"));
                                        editor.putString("user_session",jsonObj.getString("session"));
                                        editor.putString("user_pin",sha256(edit_password.getText().toString()));
                                        editor.putString("user_notif",jsonObj.getString("notif"));
                                        editor.putString("login_phone",jsonObj.getString("phone"));
                                        editor.commit();

                                        launchHomeScreen();
                                    } else if (RC.equals("02")) {
                                        Intent intent = new Intent(myApp, LoginOTPActivity.class);
                                        intent.putExtra("phone",edit_user.getText().toString());
                                        intent.putExtra("deviceid",deviceId);
                                        startActivityForResult(intent,202);
                                        Toast.makeText(myApp,jsonObj.getString("description"), Toast.LENGTH_LONG).show();
                                        progressBar.setVisibility(View.INVISIBLE);
                                    } else {
                                        Toast.makeText(myApp,jsonObj.getString("description"), Toast.LENGTH_LONG).show();
                                        progressBar.setVisibility(View.INVISIBLE);
                                    }

                                } catch (JSONException e) {
                                    Log.e("error JSON",e.getMessage());
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(myApp,"Connection Error", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });
        getThread.start();
    }

    private void launchHomeScreen() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

}