package com.tb.tokobersama;

import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tb.tokobersama.model.ResponseOTP;
import com.tb.tokobersama.model.ResponseToken;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;

import java.util.HashMap;

public class LoginOTPActivity extends BaseActivity {

    private LoginOTPActivity myApp;
    private EditText edit_otp;
    private Button btn_submit;
    private int OTP;
    private String phone, deviceId;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_otp);

        myApp = this;

        Intent intent = getIntent();
        OTP = intent.getIntExtra("reg_otp",0);
        phone = intent.getStringExtra("phone");
        deviceId = intent.getStringExtra("deviceid");

        initialize();
        getOTP();
    }

    private void initialize() {
        edit_otp = findViewById(R.id.edit_otp);
        btn_submit = findViewById(R.id.btn_submit);
        progressBar = findViewById(R.id.progress_circular);

        edit_otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                boolean isReady = edit_otp.getText().toString().length() >= 6;
                btn_submit.setEnabled(isReady);
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateOTP();
            }
        });
    }

    private void validateOTP() {
        if (Integer.parseInt(edit_otp.getText().toString())==OTP) {
            setDeviceId();
        } else {
            Toast.makeText(myApp,getString(R.string.notif_wrong_otp), Toast.LENGTH_LONG).show();
        }
    }

    private void getOTP() {
        progressBar.setVisibility(View.VISIBLE);
        getThread = new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("phone", phone);

                DataService api = RestAdapter.createAPI();

                Call<ResponseOTP> call = api.getOTP(parameters);

                call.enqueue(new Callback<ResponseOTP>() {
                    @Override
                    public void onResponse(Call<ResponseOTP> call, Response<ResponseOTP> response) {
                        ResponseOTP resp = response.body();
                        if (resp!=null && resp.RC.equals("00")) {
                            OTP = resp.otp;
                            progressBar.setVisibility(View.INVISIBLE);
                        } else {
                            Toast.makeText(myApp,resp.description, Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        getThread.interrupt();
                    }

                    @Override
                    public void onFailure(Call<ResponseOTP> call, Throwable t) {
                        Toast.makeText(myApp,"Connection Error", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.INVISIBLE);
                        getThread.interrupt();
                    }
                });
            }
        });
        getThread.start();
    }

    private void setDeviceId() {
        progressBar.setVisibility(View.VISIBLE);
        getThread = new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("phone", phone);
                parameters.put("deviceid", deviceId);

                DataService api = RestAdapter.createAPI();

                Call<ResponseToken> call = api.setDeviceId(parameters);

                call.enqueue(new Callback<ResponseToken>() {
                    @Override
                    public void onResponse(Call<ResponseToken> call, Response<ResponseToken> response) {
                        ResponseToken resp = response.body();
                        if (resp!=null && resp.RC.equals("00")) {
                            progressBar.setVisibility(View.INVISIBLE);
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK,returnIntent);
                            finish();
                            overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                        } else {
                            Toast.makeText(myApp,resp.description, Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        getThread.interrupt();
                    }

                    @Override
                    public void onFailure(Call<ResponseToken> call, Throwable t) {
                        Toast.makeText(myApp,"Connection Error", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.INVISIBLE);
                        getThread.interrupt();
                    }
                });
            }
        });
        getThread.start();
    }
}