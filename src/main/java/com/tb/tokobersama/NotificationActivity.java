package com.tb.tokobersama;

import androidx.annotation.DrawableRes;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.tb.tokobersama.adapter.AdapterNotification;
import com.tb.tokobersama.model.Notification;
import com.tb.tokobersama.model.Notifications;
import com.tb.tokobersama.model.ResponseNotification;
import com.tb.tokobersama.util.Constants;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;
import com.tb.tokobersama.util.Tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NotificationActivity extends BaseActivity {

    private SwipeRefreshLayout swipe_refresh;
    private ShimmerFrameLayout shimmer_layout;
    private Call<ResponseNotification> callbackCall = null;

    private RecyclerView recycler_view;
    private AdapterNotification mAdapter;

    private Notifications application;

    private Context myApp;

    private int count_total = 0;
    private int failed_page = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString("Notifikasi");
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        myApp = this;
        application = new Notifications();

        loadUserProfile();
        initComponent();
        initShimmerLoading();

        List<Notification> transactions = application.getNotifications();
        int _count_total = application.getCountTotal();
        if (transactions.size() == 0) {
            requestAction(1);
        } else {
            count_total = _count_total;
            displayApiResult(transactions);
        }
    }

    private void initComponent() {
        shimmer_layout = findViewById(R.id.shimmer_layout);
        swipe_refresh = findViewById(R.id.swipe_refresh);
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(myApp));
        recycler_view.setHasFixedSize(true);

        //set data and list adapter
        mAdapter = new AdapterNotification(myApp, recycler_view, new ArrayList<Notification>());
        recycler_view.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterNotification.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Notification obj, int position) {
                //ActivityVideoDetails.navigate(myApp, obj.id, false);
            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterNotification.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (count_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1);
            }
        });
    }

    private void displayApiResult(final List<Notification> items) {
        mAdapter.insertData(items);
        swipeProgress(false);
        if (items.size() == 0) showNoItemView(true, "Tidak ada notifikasi");
    }

    private void requestList(final int page_no) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("page_no", page_no + "");
        parameters.put("item_count", Constants.ITEM_PER_REQUEST + "");
        parameters.put("session", userProfile.getSession());
        parameters.put("merchant_id", userProfile.getId());

        DataService api = RestAdapter.createAPI();

        Call<ResponseNotification> call = api.getNotificationList(parameters);

        call.enqueue(new Callback<ResponseNotification>() {
            @Override
            public void onResponse(Call<ResponseNotification> call, Response<ResponseNotification> response) {
                ResponseNotification resp = response.body();
                if (resp != null && resp.RC.equals("00")) {
                    count_total = Integer.parseInt(resp.total_count);
                    displayApiResult(resp.notifications);

                    application.setCountTotal(count_total);
                    application.addNotifications(resp.notifications);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<ResponseNotification> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }
        });
    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (Tools.isConnect(myApp)) {
            showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
        } else {
            showFailedView(true, getString(R.string.no_internet_text), R.drawable.img_no_internet);
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "", R.drawable.img_failed);
        //showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
            application.resetNotification();
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestList(page_no);
            }
        }, 1000);
    }

    private void showFailedView(boolean show, String message, @DrawableRes int icon) {
        View lyt_failed = findViewById(R.id.lyt_failed);
        ((ImageView)findViewById(R.id.failed_icon)).setImageResource(icon);
        ((TextView)findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button)findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void showNoItemView(boolean show, String message) {
        View lyt_failed = findViewById(R.id.lyt_failed);
        ((ImageView)findViewById(R.id.failed_icon)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button)findViewById(R.id.failed_retry)).setVisibility(View.GONE);
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            shimmer_layout.setVisibility(View.GONE);
            shimmer_layout.stopShimmer();
            recycler_view.setVisibility(View.VISIBLE);
            return;
        }

        shimmer_layout.setVisibility(View.VISIBLE);
        shimmer_layout.startShimmer();
        recycler_view.setVisibility(View.INVISIBLE);
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    private void initShimmerLoading() {
        LinearLayout lyt_shimmer_content = findViewById(R.id.lyt_shimmer_content);
        for (int h = 0; h < 10; h++) {
            LinearLayout row_item = new LinearLayout(myApp);
            row_item.setOrientation(LinearLayout.HORIZONTAL);
            View item = getLayoutInflater().inflate(R.layout.loading_fragment_transaction, null);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 1;
            item.setLayoutParams(p);
            row_item.addView(item);
            lyt_shimmer_content.addView(row_item);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            if (!checkingSignIn()) {
                startActivity(new Intent(myApp,LoginActivity.class));
            }
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!checkingSignIn()) {
            startActivity(new Intent(myApp,LoginActivity.class));
        }
        finish();
    }
}