package com.tb.tokobersama;

import androidx.annotation.DrawableRes;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TypefaceSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.tb.tokobersama.adapter.AdapterViewPager;
import com.tb.tokobersama.model.Merchant;
import com.tb.tokobersama.model.Product;
import com.tb.tokobersama.model.ResponseMerchant;
import com.tb.tokobersama.model.ResponseProduct;
import com.tb.tokobersama.util.Constants;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;
import com.tb.tokobersama.util.Tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class ProductDetailActivity extends BaseActivity {

    private static final String EXTRA_OBJECT_ID = "key.EXTRA_OBJECT_ID";

    // activity transition
    public static void navigate(Activity activity, String id) {
        Intent i = navigateBase(activity, id);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context, String id) {
        Intent i = new Intent(context, ProductDetailActivity.class);
        i.putExtra(EXTRA_OBJECT_ID, id);
        return i;
    }

    private Product product;
    private String product_id;
    private SwipeRefreshLayout swipe_refresh;
    private ShimmerFrameLayout shimmer_layout;
    private View lyt_main_content;
    private ViewPager viewPager;
    private CardView icon_toped, icon_shopee, icon_olx, icon_instagram, icon_facebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString("Produk");
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        product_id = getIntent().getStringExtra(EXTRA_OBJECT_ID);

        initComponent();
        requestAction();
    }

    private void initComponent() {
        loadUserProfile();
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        shimmer_layout = findViewById(R.id.shimmer_layout);
        lyt_main_content = (View) findViewById(R.id.lyt_main_content);
        viewPager = findViewById(R.id.viewPager);

        icon_toped = findViewById(R.id.card_toped);
        icon_shopee = findViewById(R.id.card_shopee);
        icon_olx = findViewById(R.id.card_olx);
        icon_instagram = findViewById(R.id.card_instagram);
        icon_facebook = findViewById(R.id.card_facebook);

        // on swipe
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestAction();
            }
        });
    }

    private void requestAction() {
        showFailedView(false, "", R.drawable.img_failed);
        swipeProgress(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestProductDetailsApi();
            }
        }, 1000);
    }

    private void requestProductDetailsApi() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("id",product_id);
        parameters.put("page_no","1");
        parameters.put("item_count", Constants.ITEM_PER_REQUEST +"");
        parameters.put("session", userProfile.getSession());
        //parameters.put("merchant_id",userProfile.getId());

        DataService api = RestAdapter.createAPI();

        Call<ResponseProduct> call = api.getProductList(parameters);

        call.enqueue(new Callback<ResponseProduct>() {
            @Override
            public void onResponse(Call<ResponseProduct> call, Response<ResponseProduct> response) {
                ResponseProduct resp = response.body();
                if (resp!=null && resp.RC.equals("00")) {
                    product = resp.products.get(0);
                    displayProductDetail();
                } else {
                    onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<ResponseProduct> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest();
            }
        });
    }

    private void displayProductDetail() {
        ((TextView)findViewById(R.id.txt_price)).setText("Rp "+String.format(Locale.ITALY,"%1$,.0f", Float.parseFloat(product.price)));
        ((TextView)findViewById(R.id.txt_status)).setText(product.status);
        ((TextView)findViewById(R.id.txt_name)).setText(product.name);

        ((TextView)findViewById(R.id.txt_description)).setText(product.description);

        ArrayList<String> images = new ArrayList<>();
        if (product.picture_1!=null && !product.picture_1.equals(""))
            images.add(product.picture_1);
        if (product.picture_2!=null && !product.picture_2.equals(""))
            images.add(product.picture_2);
        if (product.picture_3!=null && !product.picture_3.equals(""))
            images.add(product.picture_3);
        if (product.picture_4!=null && !product.picture_4.equals(""))
            images.add(product.picture_4);

        AdapterViewPager adapterViewPager = new AdapterViewPager(this, images);
        viewPager.setAdapter(adapterViewPager);

        if (product.link_tokopedia!=null && !product.link_tokopedia.equals("")) {
            icon_toped.setVisibility(View.VISIBLE);
            icon_toped.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openUrl(product.link_tokopedia);
                    //openUrl("https://www.tokopedia.com/uneed-indonesia/uneed-nylon-braided-kabel-data-type-c-micro-usb-lightning-ucb43-micro-usb");
                }
            });
        }
        if (product.link_shopee!=null && !product.link_shopee.equals("")) {
            icon_shopee.setVisibility(View.VISIBLE);
            icon_shopee.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openUrl(product.link_shopee);
                }
            });
        }
        if (product.link_olx!=null && !product.link_olx.equals("")) {
            icon_olx.setVisibility(View.VISIBLE);
            icon_olx.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openUrl(product.link_olx);
                }
            });
        }
        if (product.link_instagram!=null && !product.link_instagram.equals("")) {
            icon_instagram.setVisibility(View.VISIBLE);
            icon_instagram.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openUrl(product.link_instagram);
                }
            });
        }
        if (product.link_facebook!=null && !product.link_facebook.equals("")) {
            icon_facebook.setVisibility(View.VISIBLE);
            icon_facebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openUrl(product.link_facebook);
                }
            });
        }

        requestMerchantInfo(product.merchant_id);

        lyt_main_content.setVisibility(View.VISIBLE);
        swipeProgress(false);
    }

    private void openUrl(String url) {
        Uri uri = Uri.parse(url);
        startActivity(new Intent(Intent.ACTION_VIEW, uri));
    }

    private void requestMerchantInfo(String merchant_id) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("id",merchant_id);
        parameters.put("session",userProfile.getSession());

        DataService api = RestAdapter.createAPI();

        Call<ResponseMerchant> call = api.getMerchantInfo(parameters);

        call.enqueue(new Callback<ResponseMerchant>() {
            @Override
            public void onResponse(Call<ResponseMerchant> call, Response<ResponseMerchant> response) {
                ResponseMerchant resp = response.body();
                if (resp!=null && resp.RC.equals("00")) {
                    Merchant merchant = resp.merchants.get(0);
                    ((TextView)findViewById(R.id.txt_merchant_name)).setText(merchant.store_name);
                }
            }

            @Override
            public void onFailure(Call<ResponseMerchant> call, Throwable t) {
                if (!call.isCanceled()) {

                }
            }
        });
    }

    private void onFailRequest() {
        swipeProgress(false);
        if (Tools.isConnect(this)) {
            showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
        } else {
            showFailedView(true, getString(R.string.no_internet_text), R.drawable.img_no_internet);
        }
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            shimmer_layout.setVisibility(View.GONE);
            shimmer_layout.stopShimmer();
            return;
        }
        shimmer_layout.setVisibility(View.VISIBLE);
        shimmer_layout.startShimmer();
        lyt_main_content.setVisibility(View.INVISIBLE);
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    private void showFailedView(boolean show, String message, @DrawableRes int icon) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);

        ((ImageView) findViewById(R.id.failed_icon)).setImageResource(icon);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            lyt_main_content.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_main_content.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}