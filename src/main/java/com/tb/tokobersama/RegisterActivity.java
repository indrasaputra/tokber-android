package com.tb.tokobersama;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tb.tokobersama.model.ResponseOTP;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;

import java.util.HashMap;

public class RegisterActivity extends BaseActivity {

    private RegisterActivity myApp;
    private EditText edit_full_name, edit_phone;
    private Button btn_next;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        myApp = this;

        edit_full_name = findViewById(R.id.edit_full_name);
        edit_phone = findViewById(R.id.edit_phone);
        btn_next = findViewById(R.id.btn_next);
        progressBar = findViewById(R.id.progress_circular);

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateInput()) {
                    getOTP();
                }
            }
        });

        TextView txt_signin = findViewById(R.id.txt_signin);

        txt_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private boolean validateInput() {
        boolean valid = true;

        if (TextUtils.isEmpty(edit_full_name.getText())) {
            edit_full_name.setError("Wajib diisi");
            valid = false;
            edit_full_name.requestFocus();
        }

        if (TextUtils.isEmpty(edit_phone.getText()) && edit_phone.getText().length()<10) {
            edit_phone.setError("Invalid mobile number");
            valid = false;
            edit_phone.requestFocus();
        }

//        if (isPhoneValid(edit_user.getText().toString())) {
//            edit_user.setError("No HP tidak valid");
//            valid = false;
//            edit_user.requestFocus();
//        }

        return valid;
    }

    private void getOTP() {
        progressBar.setVisibility(View.VISIBLE);
        getThread = new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("phone", edit_phone.getText().toString());

                DataService api = RestAdapter.createAPI();

                Call<ResponseOTP> call = api.getOTP(parameters);

                call.enqueue(new Callback<ResponseOTP>() {
                    @Override
                    public void onResponse(Call<ResponseOTP> call, Response<ResponseOTP> response) {
                        ResponseOTP resp = response.body();
                        if (resp!=null && resp.RC.equals("00")) {
                            launchOTP(resp.phone, resp.otp, edit_full_name.getText().toString());
                            progressBar.setVisibility(View.INVISIBLE);
                        } else {
                            Toast.makeText(myApp,resp.description, Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseOTP> call, Throwable t) {
                        Toast.makeText(myApp,"Connection Error", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });
        getThread.start();
    }

    private void launchOTP(String phone, int OTP, String name) {
        Intent intent = new Intent(RegisterActivity.this, RegisterOTPActivity.class);
        intent.putExtra("reg_phone",phone);
        intent.putExtra("reg_otp",OTP);
        intent.putExtra("reg_name",name);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
    }

}