package com.tb.tokobersama;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterOTPActivity extends AppCompatActivity {

    private RegisterOTPActivity myApp;
    private EditText edit_otp;
    private Button btn_next;
    private int OTP;
    private String phone, name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_otp);

        myApp = this;

        Intent intent = getIntent();
        OTP = intent.getIntExtra("reg_otp",0);
        phone = intent.getStringExtra("reg_phone");
        name = intent.getStringExtra("reg_name");

        edit_otp = findViewById(R.id.edit_otp);
        btn_next = findViewById(R.id.btn_next);

        edit_otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                boolean isReady = edit_otp.getText().toString().length() >= 6;
                btn_next.setEnabled(isReady);
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateOTP();
            }
        });

        TextView txt_signin = findViewById(R.id.txt_signin);

        txt_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RegisterOTPActivity.this,LoginActivity.class));
                finish();
            }
        });
    }

    private void validateOTP() {
        if (Integer.parseInt(edit_otp.getText().toString())==OTP) {
            Intent intent = new Intent(RegisterOTPActivity.this,RegisterPasswordActivity.class);
            intent.putExtra("reg_phone",phone);
            intent.putExtra("reg_name",name);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        } else {
            Toast.makeText(myApp,getString(R.string.notif_wrong_otp), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }
}