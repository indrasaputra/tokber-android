package com.tb.tokobersama;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tb.tokobersama.model.ResponseRegister;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;

import java.util.HashMap;

public class RegisterPasswordActivity extends BaseActivity {

    private RegisterPasswordActivity myApp;
    private EditText edit_pin, edit_repin;
    private Button btn_submit;
    private ProgressBar progressBar;
    private String name, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_password);

        myApp = this;

        Intent intent = getIntent();
        name = intent.getStringExtra("reg_name");
        phone = intent.getStringExtra("reg_phone");

        edit_pin = findViewById(R.id.edit_pin);
        edit_repin = findViewById(R.id.edit_re_pin);
        progressBar = findViewById(R.id.progress_circular);
        btn_submit = findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateInput()) {
                    callRegister();
                }
            }
        });
    }

    private boolean validateInput() {
        boolean valid = true;

        if (TextUtils.isEmpty(edit_pin.getText()) || edit_pin.getText().length()<6) {
            edit_pin.setError("Wajib diisi 6 digit");
            valid = false;
            edit_pin.requestFocus();
        }

        if (TextUtils.isEmpty(edit_repin.getText()) || edit_repin.getText().length()<6) {
            edit_repin.setError("Wajib diisi 6 digit");
            valid = false;
            edit_repin.requestFocus();
        }

        if (!edit_repin.getText().toString().equals(edit_pin.getText().toString())) {
            edit_repin.setError("PIN not matched");
            valid = false;
            edit_repin.requestFocus();
        }

        return valid;
    }

    private void callRegister() {
        progressBar.setVisibility(View.VISIBLE);
        getThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("callRegister","call");
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("phone", phone);
                parameters.put("name", name);
                parameters.put("passwd", sha256(edit_pin.getText().toString()));

                DataService api = RestAdapter.createAPI();

                Call<ResponseRegister> call = api.register(parameters);

                call.enqueue(new Callback<ResponseRegister>() {
                    @Override
                    public void onResponse(Call<ResponseRegister> call, Response<ResponseRegister> response) {
                        ResponseRegister resp = response.body();
                        if (resp!=null && resp.RC.equals("00")) {
                            int TIME_OUT = 2000;
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    launchLogin();
                                }
                            }, TIME_OUT);
                            Toast.makeText(myApp,resp.description, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(myApp,resp.description, Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseRegister> call, Throwable t) {
                        Toast.makeText(myApp,"Connection Error", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });
        getThread.start();
    }

    private void launchLogin() {
        startActivity(new Intent(RegisterPasswordActivity.this,LoginActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
    }

}