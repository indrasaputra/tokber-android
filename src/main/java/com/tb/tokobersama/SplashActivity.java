package com.tb.tokobersama;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Intent intent = getIntent();
        if (intent.getStringExtra("Notifikasi")!=null) {
            Log.e("Splash extra", intent.getStringExtra("Notifikasi"));
        } else {
            Log.e("Splash extra", "nulllllll");
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //bundle must contain all info sent in "data" field of the notification
            ArrayList list = bundle.getStringArrayList("data");
            Log.e("Bundle not null",list.get(0).toString());
        } else {
            Log.e("Bundle Splash","nullllll");
        }

        int SPLASH_TIME_OUT = 1000;
        new Handler().postDelayed(new Runnable() {

                                      /*
                                       * Showing splash screen with a timer. This will be useful when you
                                       * want to show case your app logo / company
                                       */

                                      @Override
                                      public void run() {
                                          launchLogin();
                                      }
                                  },
                SPLASH_TIME_OUT);
    }

    private void launchLogin() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }
}