package com.tb.tokobersama;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tb.tokobersama.model.ResponseService;
import com.tb.tokobersama.model.ResponseTopup;
import com.tb.tokobersama.model.Service;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;
import com.tb.tokobersama.util.Tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.R.layout.simple_spinner_item;

public class TopupActivity extends BaseActivity {

    private static final String EXTRA_OBJECT_MENU = "menu";
    private static final String EXTRA_OBJECT_TITLE = "title";


    // activity transition
    public static void navigate(Activity activity, String title, String menu) {
        Intent i = navigateBase(activity, title, menu);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context, String title, String menu) {
        Intent i = new Intent(context, ProductDetailActivity.class);
        i.putExtra(EXTRA_OBJECT_TITLE, title);
        i.putExtra(EXTRA_OBJECT_MENU, menu);
        return i;
    }

    private Spinner sp_provider;
    private Spinner sp_voucher;
    private EditText et_number;
    private TextView text_price;

    private ArrayList<String> voucherList = new ArrayList<>();
    private List<Service> services = new ArrayList<>();
    private String[] categories;

    private TopupActivity myApp;

    private String menuType, title, code, price, message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);

        myApp = this;

        getBundle();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableString s = new SpannableString(title);
        s.setSpan(new TypefaceSpan("MyTypeface.otf"), 0, s.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        getSupportActionBar().setTitle(
                (Html.fromHtml("<font color=\"#ffffff\">" + s + "</font>")));

        initiateComponent();
    }

    private void getBundle() {
        Intent intent = getIntent();
        title = intent.getStringExtra(EXTRA_OBJECT_TITLE);
        menuType = intent.getStringExtra(EXTRA_OBJECT_MENU);
    }

    private void initiateComponent() {
        loadUserProfile();

        sp_provider = findViewById(R.id.sp_provider);
        sp_voucher = findViewById(R.id.sp_voucher);
        et_number = findViewById(R.id.et_number);
        text_price = findViewById(R.id.text_price);

        if (menuType.equals("pulsa")) {
            setSp_provider(getResources().getStringArray(R.array.operator_pulsa));
            categories = getResources().getStringArray(R.array.category_pulsa);
        } else if (menuType.equals("data")) {
            setSp_provider(getResources().getStringArray(R.array.operator_data));
            categories = getResources().getStringArray(R.array.category_data);
        } else if (menuType.equals("emoney")) {
            setSp_provider(getResources().getStringArray(R.array.operator_emoney));
            categories = getResources().getStringArray(R.array.category_emoney);
        }

        sp_provider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i==0) {
                    voucherList.clear();
                    sp_voucher.setAdapter(null);
                    text_price.setVisibility(View.INVISIBLE);
                } else {
                    requestCategory(categories[i-1]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final Button btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitTopup();
            }
        });
    }

    private void setSp_provider(String[] providers) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, simple_spinner_item, providers);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_provider.setAdapter(spinnerArrayAdapter);
    }

    private void requestCategory(String category) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("session", userProfile.getSession());
        parameters.put("category", category);

        DataService api = RestAdapter.createAPI();

        Call<ResponseService> call = api.getService(parameters);

        call.enqueue(new Callback<ResponseService>() {
            @Override
            public void onResponse(Call<ResponseService> call, Response<ResponseService> response) {
                ResponseService resp = response.body();
                if (resp!=null && resp.RC.equals("00")) {
                    services = resp.service;
                    voucherList = new ArrayList<>();
                    for (int i=0; i<services.size();i++) {
                        voucherList.add(services.get(i).description);
                    }
                    setSp_voucher();
                }
            }

            @Override
            public void onFailure(Call<ResponseService> call, Throwable t) {

            }
        });
    }

    private void setSp_voucher() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, simple_spinner_item, voucherList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        sp_voucher.setAdapter(spinnerArrayAdapter);

        sp_voucher.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                price = "Rp "+String.format(Locale.ITALY,"%1$,.0f", Float.parseFloat(services.get(i).sell_price));
                code = services.get(i).code;
                text_price.setText(price);
                text_price.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void submitTopup() {
        View focusView = null;
        boolean cancel = false;

        if (TextUtils.isEmpty(et_number.getText())) {
            et_number.setError(getResources().getString(R.string.notif_mandatory));
            focusView = et_number;
            cancel = true;
        }
        if(sp_provider.getSelectedItemPosition()==0) {
            Toast.makeText(myApp,"Silakan pilih provider", Toast.LENGTH_LONG).show();
            focusView = sp_provider;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showPinDialog();
        }
    }

    private void showPinDialog() {
        final AlertDialog pinAuth = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = LayoutInflater.from(this);
        final View dialogView = inflater.inflate(R.layout.layout_topup_pin_dialog, null);
        pinAuth.setCancelable(true);
        pinAuth.setView(dialogView);

        TextView text_title = dialogView.findViewById(R.id.text_title);
        TextView text_provider_name = dialogView.findViewById(R.id.text_bank_name);
        TextView text_voucher_desc = dialogView.findViewById(R.id.text_bank_acc);
        TextView text_nominal = dialogView.findViewById(R.id.text_nominal);
        TextView text_acc_name = dialogView.findViewById(R.id.text_acc_name);

        text_title.setText(title);
        text_provider_name.setText(sp_provider.getSelectedItem().toString());
        text_voucher_desc.setText(sp_voucher.getSelectedItem().toString());
        text_acc_name.setText(et_number.getText().toString());
        text_nominal.setText(text_price.getText());

        final EditText pin = (EditText) dialogView.findViewById(R.id.et_pin);
        //pin.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        final Button btn_ok = dialogView.findViewById(R.id.btn_ok);

        pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                boolean isReady = pin.getText().toString().length() >= 6;
                btn_ok.setEnabled(isReady);
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("PIN", pin.getText().toString()+" vs "+userProfile.getPin());
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) myApp.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(dialogView.getWindowToken(), 0);
                if (sha256(pin.getText().toString()).equals(userProfile.getPin())) {
                    requestTopup();
                    pinAuth.dismiss();
                } else {
                    Toast.makeText(myApp,"PIN salah", Toast.LENGTH_LONG).show();
                }
            }
        });

        pinAuth.show();
    }

    private void requestTopup() {
        final ProgressBar pb = findViewById(R.id.progress_circular);
        pb.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        getThread = new Thread(new Runnable() {
            @Override
            public void run() {
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("merchant_id",userProfile.getId());
                parameters.put("session",userProfile.getSession());
                parameters.put("trxid", Tools.getTrxid(userProfile.getId()));
                parameters.put("product",code);
                parameters.put("idpel",et_number.getText().toString());
                parameters.put("pin",userProfile.getPin());

                DataService api = RestAdapter.createAPI();

                Call<ResponseTopup> call = api.requestTopup(parameters);

                call.enqueue(new Callback<ResponseTopup>() {
                    @Override
                    public void onResponse(Call<ResponseTopup> call, Response<ResponseTopup> response) {
                        ResponseTopup resp = response.body();
                        if (resp!=null) {
                            if (resp.rc.equals("1000") || resp.rc.equals("0000") || resp.rc.equals("00")) {
                                showPromptDialog(true, resp.text);
                            } else {
                                showPromptDialog(false,resp.description);
                            }
                        } else {
                            Toast.makeText(myApp,"Server Error", Toast.LENGTH_LONG).show();
                            getThread.interrupt();
                        }
                        pb.setVisibility(View.INVISIBLE);
                        getThread.interrupt();
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }

                    @Override
                    public void onFailure(Call<ResponseTopup> call, Throwable t) {
                        pb.setVisibility(View.INVISIBLE);
                        Toast.makeText(myApp,"Connection Error", Toast.LENGTH_LONG).show();
                        getThread.interrupt();
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }
                });
            }
        });
        getThread.start();
    }

    private void showPromptDialog(boolean successful, String message) {
        final boolean success = successful;
        final AlertDialog prompt = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = LayoutInflater.from(this);
        final View dialogView = inflater.inflate(R.layout.layout_topup_dialog, null);
        prompt.setCancelable(false);
        prompt.setView(dialogView);
        prompt.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView text_title = dialogView.findViewById(R.id.text_title);
        TextView text_provider = dialogView.findViewById(R.id.text_bank_name);
        TextView text_voucher = dialogView.findViewById(R.id.text_bank_acc);
        TextView text_tujuan = dialogView.findViewById(R.id.text_acc_name);
        TextView text_harga = dialogView.findViewById(R.id.text_nominal);
        TextView text_status = dialogView.findViewById(R.id.text_result);
        TextView text_pesan = dialogView.findViewById(R.id.text_pesan);
        ImageView img_status = dialogView.findViewById(R.id.img_status);

        text_title.setText(title);
        text_provider.setText(sp_provider.getSelectedItem().toString());
        text_voucher.setText(sp_voucher.getSelectedItem().toString());
        text_tujuan.setText(et_number.getText());
        text_harga.setText(text_price.getText());
        text_pesan.setText(message);

        if (success) {
            text_status.setText("BERHASIL");
            img_status.setImageDrawable(getResources().getDrawable(R.drawable.icon_success));
        } else {
            text_status.setText("GAGAL");
            img_status.setImageDrawable(getResources().getDrawable(R.drawable.icon_fail));
        }

        Button btn_ok = dialogView.findViewById(R.id.btn_done);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (success) {
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                } else {
                    prompt.dismiss();
                }
            }
        });

        prompt.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}