package com.tb.tokobersama.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tb.tokobersama.AddProductActivity;
import com.tb.tokobersama.R;
import com.tb.tokobersama.TopupActivity;
import com.tb.tokobersama.model.MainMenu;

import java.util.ArrayList;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterMainMenu extends RecyclerView.Adapter<AdapterMainMenu.MyViewHolder>{
    private Context mContext;
    private ArrayList<MainMenu> mainMenuDataSourceList;
    private Fragment fragment;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView icon;
        public TextView title;
        public CardView cardView;
        public RelativeLayout backgroundColour;

        public MyViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.card_view);
            icon = view.findViewById(R.id.icon);
            title = view.findViewById(R.id.title);
            backgroundColour = view.findViewById(R.id.backgroundColour);
        }
    }

    public AdapterMainMenu(Context mContext, ArrayList<MainMenu> mainMenuDataSourceList) {
        this.mContext = mContext;
        this.mainMenuDataSourceList = mainMenuDataSourceList;
    }

    public AdapterMainMenu(Fragment fragment, ArrayList<MainMenu> mainMenuDataSourceList) {
        this.mainMenuDataSourceList = mainMenuDataSourceList;
        this.fragment = fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_menu, parent, false);
        mContext = parent.getContext();
        return new MyViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return mainMenuDataSourceList.size();
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final MainMenu mainMenuDataSource = mainMenuDataSourceList.get(position);

        holder.title.setText(mainMenuDataSource.getTitle());
        holder.title.setTextColor(ContextCompat.getColor(mContext, mainMenuDataSource.getTextColor()));
        holder.icon.setImageResource(mainMenuDataSource.getIcon());
        holder.backgroundColour.setBackgroundResource(mainMenuDataSource.getBackgroundColor());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainMenuDataSource.getId() == 1) {
//                    Intent intent = new Intent(mContext, AddProductActivity.class);
//                    intent.putExtra("fromHome",true);
//                    mContext.startActivity(intent);
                    fragment.startActivityForResult(new Intent(mContext, AddProductActivity.class),101);
                } else if (mainMenuDataSource.getId() == 2) {
                    Intent intent = new Intent(mContext, TopupActivity.class);
                    intent.putExtra("menu", "pulsa");
                    intent.putExtra("title", "Pulsa");
                    fragment.startActivityForResult(intent, 102);
                } else if (mainMenuDataSource.getId() == 3) {
                    Intent intent = new Intent(mContext, TopupActivity.class);
                    intent.putExtra("menu", "data");
                    intent.putExtra("title", "Paket Data");
                    fragment.startActivityForResult(intent, 102);
                } else if (mainMenuDataSource.getId() == 4) {
                    Intent intent = new Intent(mContext, TopupActivity.class);
                    intent.putExtra("menu", "emoney");
                    intent.putExtra("title", "Uang Elektronik");
                    fragment.startActivityForResult(intent, 102);
                } else {
                    Toast.makeText(mContext, mainMenuDataSource.getTitle()+" tidak tersedia", Toast.LENGTH_LONG ).show();
                }
            }
        });
    }


}
