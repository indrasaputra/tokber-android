package com.tb.tokobersama.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tb.tokobersama.R;
import com.tb.tokobersama.model.Transaction;
import com.tb.tokobersama.util.Constants;
import com.tb.tokobersama.util.Tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterTransaction extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<Transaction> items = new ArrayList<>();

    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    private Context ctx;
    private AdapterTransaction.OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, Transaction obj, int position);
    }

    public void setOnItemClickListener(final AdapterTransaction.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterTransaction(Context context, RecyclerView view, List<Transaction> items) {
        this.items = items;
        ctx = context;
        lastItemViewDetector(view);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_date;
        public TextView txt_type;
        public TextView txt_description;
        public TextView txt_amount;
        public View lyt_parent;

        public OriginalViewHolder(View v) {
            super(v);
            txt_date = v.findViewById(R.id.txt_date);
            txt_type = v.findViewById(R.id.txt_type);
            txt_description = v.findViewById(R.id.txt_description);
            txt_amount = v.findViewById(R.id.txt_amount);
            lyt_parent = v.findViewById(R.id.lyt_parent);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress_loading);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_transaction, parent, false);
            vh = new AdapterTransaction.OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new AdapterTransaction.ProgressViewHolder(v);
        }
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof AdapterTransaction.OriginalViewHolder) {
            final Transaction p = items.get(position);
            AdapterTransaction.OriginalViewHolder vItem = (AdapterTransaction.OriginalViewHolder) holder;
            vItem.txt_date.setText(Tools.formatDateTime(p.tr_date));
            vItem.txt_type.setText(p.status);
            String op = "";
            if (p.type.equalsIgnoreCase("debit")) {
                op = "-";
            } else {
                op = "+";
            }
            if (p.status.equalsIgnoreCase("success")) {
                vItem.txt_type.setBackground(ctx.getResources().getDrawable(R.drawable.shape_round_green_overlay));
            } else if (p.status.equalsIgnoreCase("process")) {
                vItem.txt_type.setBackground(ctx.getResources().getDrawable(R.drawable.shape_round_orange_overlay));
            } else {
                vItem.txt_type.setBackground(ctx.getResources().getDrawable(R.drawable.shape_round_red_overlay));
            }

            vItem.txt_description.setText(p.description);
            vItem.txt_amount.setText(op+String.format(Locale.ITALY,"%1$,.0f", Float.parseFloat(p.amount)));

            vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, p, position);
                    }
                }
            });
        } else {
            ((AdapterTransaction.ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return this.items.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void insertData(List<Transaction> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }

    public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }
    }

    public void setLoading() {
        if (getItemCount() != 0) {
            this.items.add(null);
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }

    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = layoutManager.findLastVisibleItemPosition();
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        if (onLoadMoreListener != null) {
                            int current_page = getItemCount() / Constants.ITEM_PER_REQUEST;
                            onLoadMoreListener.onLoadMore(current_page);
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }

}
