package com.tb.tokobersama.model;

import java.util.ArrayList;
import java.util.List;

public class Downlines {
    private int countTotal = 0;

    private List<Downline> downlines = new ArrayList<>();

    public void setCountTotal(int countTotal) {
        this.countTotal = countTotal;
    }

    public void addItems(List<Downline> downlines) {
        this.downlines.addAll(downlines);
    }

    public void resetItems() {
        this.downlines.clear();
    }

    public List<Downline> getItems() {
        return downlines;
    }

    public int getCountTotal() {
        return countTotal;
    }
}
