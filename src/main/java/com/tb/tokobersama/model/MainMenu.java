package com.tb.tokobersama.model;

public class MainMenu {

    //body
    private String title;
    private int icon;
    private int backgroundColor;
    private int textColor;
    private int id;

    //body
    public MainMenu(int id, String title, int icon, int backgroundColor, int textColor) {
        this.title = title;
        this.icon = icon;
        this.backgroundColor = backgroundColor;
        this.textColor = textColor;
        this.id = id;
    }

    //-------------------body get--------------
    public String getTitle(){
        return this.title;
    }
    public int getIcon(){
        return icon;
    }
    public int getBackgroundColor(){
        return this.backgroundColor;
    }
    public int getTextColor(){
        return this.textColor;
    }
    public int getId(){
        return id;
    }
}
