package com.tb.tokobersama.model;

import java.io.Serializable;

public class Merchant implements Serializable {
    public String id;
    public String phone="";
    public String name="";
    public String address="";
    public String province="";
    public String city="";
    public String email="";
    public String store_name="";
    public String store_description="";
    public String balance="";
    public String reg_date="";
    public String upline="";
    public String lon="";
    public String lat="";
    public String bank_name="";
    public String account_name="";
    public String bank_account_number="";
    public String postal_code="";
    public String status="";
    public String session="";
}
