package com.tb.tokobersama.model;

import java.io.Serializable;

public class Notification implements Serializable {
    public String id="";
    public String date="";
    public String title="";
    public String content="";
    public String merchant_id="";
    public String status="";
}
