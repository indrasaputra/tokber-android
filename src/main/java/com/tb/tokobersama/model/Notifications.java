package com.tb.tokobersama.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Notifications implements Serializable {
    private int countTotal = 0;
    private List<Notification> notifications = new ArrayList<>();

    public void setCountTotal(int countTotal) {
        this.countTotal = countTotal;
    }

    public void addNotifications(List<Notification> notifications) {
        this.notifications.addAll(notifications);
    }

    public void resetNotification() {
        this.notifications.clear();
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public int getCountTotal() {
        return countTotal;
    }
}
