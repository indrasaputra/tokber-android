package com.tb.tokobersama.model;

import java.io.Serializable;

public class Product implements Serializable {
    public String id = "";
    public String merchant_id = "";
    public String name="";
    public String description="";
    public String stock="";
    public String category_id="";
    public String picture_1="";
    public String picture_2="";
    public String picture_3="";
    public String picture_4="";
    public String store_name="";

    public String sku="";
    public String reg_date="";
    public String reg_by="";
    public String last_update="";
    public String last_update_by="";
    public String link_tokopedia="";
    public String link_shopee="";
    public String link_tokber="";
    public String link_olx="";
    public String link_instagram="";
    public String link_facebook="";
    public String price="";
    public String status="";
}
