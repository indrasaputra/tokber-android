package com.tb.tokobersama.model;

import java.util.ArrayList;
import java.util.List;

public class Products {
    private int countTotalProduct = 0;
    private List<Product> products = new ArrayList<>();

    public void setCountTotalProduct(int countTotalProduct) {
        this.countTotalProduct = countTotalProduct;
    }

    public void addProducts(List<Product> products) {
        this.products.addAll(products);
    }

    public void resetProducts() {
        this.products.clear();
    }

    public List<Product> getProducts() {
        return products;
    }

    public int getCountTotalProduct() {
        return countTotalProduct;
    }
}
