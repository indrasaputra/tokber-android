package com.tb.tokobersama.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseCategory implements Serializable {
    public String RC="";
    public List<Category> categories = new ArrayList<>();
}
