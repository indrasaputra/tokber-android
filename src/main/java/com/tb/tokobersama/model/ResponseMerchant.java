package com.tb.tokobersama.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseMerchant implements Serializable {
    public String RC = "";
    public List<Merchant> merchants = new ArrayList<>();
}
