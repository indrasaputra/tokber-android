package com.tb.tokobersama.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseNotification implements Serializable {
    public String RC="";
    public String total_count="";
    public String unread="";
    public List<Notification> notifications = new ArrayList<>();
}
