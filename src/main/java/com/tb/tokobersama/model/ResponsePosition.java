package com.tb.tokobersama.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponsePosition implements Serializable {
    public String RC="";
    public String total_count="";
    public List<Downline> downline = new ArrayList<>();
}
