package com.tb.tokobersama.model;

import java.io.Serializable;

public class ResponseTopup implements Serializable {
    public String rc="";
    public String description="";
    public String ResponseCode="";
    public String serialnumber="";
    public String ACK="";
    public String nomor="";
    public String trxid="";
    public String pesan="";
    public String refnum="";
    public int nominal=0;
    public int harga=0;
    public String voucherid="";
    public String text="";
    public String tanggal="";
    public String rcm="";
    public String status="";

}
