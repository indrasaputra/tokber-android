package com.tb.tokobersama.model;

import java.io.Serializable;

public class Service implements Serializable {
    public String name="";
    public String description="";
    public String type="";
    public String buy_price="";
    public String sell_price="";
    public String code="";
    public String category="";
    public String status="";
}
