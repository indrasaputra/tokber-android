package com.tb.tokobersama.model;

import java.io.Serializable;

public class Transaction implements Serializable {
    public String id="";
    public String tr_date="";
    public String service_id="";
    public String merchant_id="";
    public String amount="";
    public String type="";
    public String description="";
    public String status="";

}
