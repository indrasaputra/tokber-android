package com.tb.tokobersama.model;

import java.util.ArrayList;
import java.util.List;

public class Transactions {
    private int countTotal = 0;
    private List<Transaction> transactions = new ArrayList<>();

    public void setCountTotal(int countTotal) {
        this.countTotal = countTotal;
    }

    public void addItems(List<Transaction> transactions) {
        this.transactions.addAll(transactions);
    }

    public void resetItems() {
        this.transactions.clear();
    }

    public List<Transaction> getItems() {
        return transactions;
    }

    public int getCountTotal() {
        return countTotal;
    }

}
