package com.tb.tokobersama.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.tb.tokobersama.DisbursementActivity;
import com.tb.tokobersama.MainActivity;
import com.tb.tokobersama.NotificationActivity;
import com.tb.tokobersama.ProductDetailActivity;
import com.tb.tokobersama.R;
import com.tb.tokobersama.WebViewActivity;
import com.tb.tokobersama.adapter.AdapterMainMenu;
import com.tb.tokobersama.adapter.AdapterProductAll;
import com.tb.tokobersama.model.MainMenu;
import com.tb.tokobersama.model.Merchant;
import com.tb.tokobersama.model.Product;
import com.tb.tokobersama.model.Products;
import com.tb.tokobersama.model.ResponseMerchant;
import com.tb.tokobersama.model.ResponseNotification;
import com.tb.tokobersama.model.ResponseProduct;
import com.tb.tokobersama.model.UserProfile;
import com.tb.tokobersama.ui.home.HomeViewModel;
import com.tb.tokobersama.util.Constants;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;
import com.tb.tokobersama.util.Tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private SharedPreferences prefs;
    private String user_phone, user_name, user_balance, saldo;
    private TextView text_name, text_saldo, text_count;
    private ProgressBar progress_balance;
    private View rootView;
    private UserProfile userProfile;
    private RecyclerView recyclerMainMenu;
    private AdapterMainMenu adapterMainMenu;
    private ArrayList<MainMenu> mainMenuList;
    private Context myApp;

    private SwipeRefreshLayout swipe_refresh;
    private ShimmerFrameLayout shimmer_product;
    private Call<ResponseProduct> callbackCall = null;

    private RecyclerView recycler_view;
    private AdapterProductAll mAdapter;

    private Products application;

    private int count_total = 0;
    private int failed_page = 0;

    private boolean hasLoad = false;

    public static HomeFragment newInstance(UserProfile userProfile) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putSerializable("UserProfile", userProfile);
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
//        //homeViewModel =
//                ViewModelProviders.of(this).get(HomeViewModel.class);
        rootView = inflater.inflate(R.layout.fragment_home, container, false);

//        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });


        initialize();
        setVariables();
        prepareMainMenu();
        initShimmerLoading();
        requestMerchantInfo();
        requestUnreadNotifCount();

        List<Product> products = application.getProducts();
        int _count_total = application.getCountTotalProduct();
        if (products.size() == 0) {
            requestAction(1);
        } else {
            count_total = _count_total;
            displayApiResult(products);
        }

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            userProfile = (UserProfile) getArguments().getSerializable("UserProfile");
//        }
        //userProfile = ((MainActivity)getActivity()).getUserProfile();
        myApp = getActivity();
        application = new Products();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("Activity Result",requestCode+" "+resultCode);
        if (requestCode==101) {
            if (resultCode==Activity.RESULT_OK) {
                mAdapter.resetListData();
                requestAction(1);
                requestMerchantInfo();
                requestUnreadNotifCount();
            }
        } else if (requestCode==102) {
            requestMerchantInfo();
            requestUnreadNotifCount();
        }
    }

    private void initialize() {
        userProfile = Tools.getUserProfile(myApp);
        text_name = rootView.findViewById(R.id.text_name);
        text_saldo = rootView.findViewById(R.id.text_saldo);
        recyclerMainMenu = rootView.findViewById(R.id.recyler_main_menu);
        progress_balance = rootView.findViewById(R.id.progress_balance);
        text_count = rootView.findViewById(R.id.text_count);

        mainMenuList = new ArrayList<>();
        //adapterMainMenu = new AdapterMainMenu(myApp, mainMenuList);
        adapterMainMenu = new AdapterMainMenu(this, mainMenuList);
        GridLayoutManager mLayoutManager = new GridLayoutManager(myApp, 4);
        recyclerMainMenu.setLayoutManager(mLayoutManager);
        recyclerMainMenu.setAdapter(adapterMainMenu);

        shimmer_product = rootView.findViewById(R.id.shimmer_product);
        swipe_refresh = rootView.findViewById(R.id.swipe_refresh);
        recycler_view = rootView.findViewById(R.id.recycler_view);
        GridLayoutManager pLayoutManager = new GridLayoutManager(myApp, 2);
        recycler_view.setLayoutManager(pLayoutManager);
        recycler_view.setHasFixedSize(true);

        //set data and list adapter
        //NestedScrollView scroll = rootView.findViewById(R.id.scroll);
        //mAdapter = new AdapterProductAll(myApp, scroll, recycler_view, new ArrayList<Product>());
        mAdapter = new AdapterProductAll(myApp, recycler_view, new ArrayList<Product>());
        recycler_view.setAdapter(mAdapter);
        //recycler_view.setNestedScrollingEnabled(false);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterProductAll.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Product obj, int position) {
                ProductDetailActivity.navigate((Activity)myApp, obj.id);
            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterProductAll.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (count_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1);
                requestMerchantInfo();
            }
        });

        ImageView icon_topup = rootView.findViewById(R.id.icon_topup);
        icon_topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(myApp, WebViewActivity.class);
                intent.putExtra("title","Top Up");
                intent.putExtra("url","file:///android_asset/topup.html");
                startActivity(intent);
            }
        });

        ImageView icon_disburse = rootView.findViewById(R.id.icon_disburse);
        icon_disburse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(myApp, DisbursementActivity.class);
                startActivityForResult(intent,102);
            }
        });

        ImageView img_notif = rootView.findViewById(R.id.img_notification);
        img_notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(myApp, NotificationActivity.class));
            }
        });
    }

    private void setVariables() {
        prefs = ((MainActivity)getActivity()).prefs;

        user_phone = prefs.getString("user_phone",null);
        user_name = prefs.getString("user_name",null);
        user_balance = prefs.getString("user_balance","0");

        if (user_balance.equals("null"))
            user_balance = "0";
        Log.e("user_balance",user_balance);
        saldo = String.format(Locale.ITALY,"%1$,.0f", Float.parseFloat(user_balance));

        text_name.setText(user_name);
        text_saldo.setText(saldo);
    }

    private void prepareMainMenu() {
        int[] covers = new int[] {
                R.drawable.icon_add_product,
                R.drawable.icon_handphone,
                R.drawable.icon_data,
                R.drawable.icon_emoney,
                R.drawable.icon_electricity,
                R.drawable.icon_electricity
        };

        MainMenu a = null;
        if (userProfile.getStatus().equalsIgnoreCase("active")) {
            a = new MainMenu(1, "Upload Produk", covers[0], R.color.design_default_color_background, android.R.color.black);
            mainMenuList.add(a);
        } else {
            a = new MainMenu(-1, "Upload Produk", covers[0], R.color.design_default_color_background, android.R.color.black);
            mainMenuList.add(a);
        }
        a = new MainMenu(2, "Pulsa", covers[1], R.color.design_default_color_background, android.R.color.black);
        mainMenuList.add(a);
        a = new MainMenu(3, "Paket Data", covers[2], R.color.design_default_color_background, android.R.color.black);
        mainMenuList.add(a);
        a = new MainMenu(4, "Uang Elektronik", covers[3], R.color.design_default_color_background, android.R.color.black);
        mainMenuList.add(a);
//        a = new MainMenu(5, "Listrik", covers[4], R.color.design_default_color_background, android.R.color.black);
//        mainMenuList.add(a);
//        a = new MainMenu(4, "Uang Elektronik", covers[5], R.color.design_default_color_background, android.R.color.black);
//        mainMenuList.add(a);

        adapterMainMenu.notifyDataSetChanged();
    }

    private void displayApiResult(final List<Product> items) {
        mAdapter.insertData(items);
        swipeProgress(false);
        if (items.size() == 0) showNoItemView(true, "");
    }

    private void requestListProductList(final int page_no) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("page_no",page_no+"");
        parameters.put("item_count", Constants.ITEM_PER_REQUEST +"");
        parameters.put("session",userProfile.getSession());
        //parameters.put("merchant_id",userProfile.getUser_id());

        DataService api = RestAdapter.createAPI();

        Call<ResponseProduct> call = api.getProductList(parameters);

        call.enqueue(new Callback<ResponseProduct>() {
            @Override
            public void onResponse(Call<ResponseProduct> call, Response<ResponseProduct> response) {
                ResponseProduct resp = response.body();
                if (resp!=null && resp.RC.equals("00")) {
                    count_total = Integer.parseInt(resp.total_count);
                    displayApiResult(resp.products);

                    application.setCountTotalProduct(count_total);
                    application.addProducts(resp.products);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<ResponseProduct> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }
        });
    }

    private void requestUnreadNotifCount() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("page_no", "1");
        parameters.put("item_count", "1");
        parameters.put("session", userProfile.getSession());
        parameters.put("merchant_id", userProfile.getId());

        DataService api = RestAdapter.createAPI();

        Call<ResponseNotification> call = api.getNotificationList(parameters);

        call.enqueue(new Callback<ResponseNotification>() {
            @Override
            public void onResponse(Call<ResponseNotification> call, Response<ResponseNotification> response) {
                ResponseNotification resp = response.body();
                if (resp != null && resp.RC.equals("00")) {
                    if (!resp.unread.equals("0")) {
                        text_count.setText(resp.unread);
                        text_count.setVisibility(View.VISIBLE);
                    } else {
                        text_count.setVisibility(View.INVISIBLE);
                    }
                } else {
                    text_count.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponseNotification> call, Throwable t) {
                text_count.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void requestMerchantInfo() {
        text_saldo.setVisibility(View.GONE);
        progress_balance.setVisibility(View.VISIBLE);

        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("id",userProfile.getId());
        parameters.put("session",userProfile.getSession());

        DataService api = RestAdapter.createAPI();

        Call<ResponseMerchant> call = api.getMerchantInfo(parameters);

        call.enqueue(new Callback<ResponseMerchant>() {
            @Override
            public void onResponse(Call<ResponseMerchant> call, Response<ResponseMerchant> response) {
                ResponseMerchant resp = response.body();
                if (resp!=null && resp.RC.equals("00")) {
                    Merchant merchant = resp.merchants.get(0);
                    String saldo = null;
                    if(merchant.balance==null)
                        saldo = "0";
                    else
                        saldo = merchant.balance;
                    text_saldo.setText(String.format(Locale.ITALY,"%1$,.0f", Float.parseFloat(saldo)));
                    progress_balance.setVisibility(View.GONE);
                    text_saldo.setVisibility(View.VISIBLE);

                } else {
                    progress_balance.setVisibility(View.GONE);
                    text_saldo.setText("-");
                    text_saldo.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponseMerchant> call, Throwable t) {
                if (!call.isCanceled()) {
                    progress_balance.setVisibility(View.GONE);
                    text_saldo.setText("-");
                    text_saldo.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (Tools.isConnect(myApp)) {
            showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
        } else {
            showFailedView(true, getString(R.string.no_internet_text), R.drawable.img_no_internet);
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "", R.drawable.img_failed);
        //showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
            application.resetProducts();
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestListProductList(page_no);
            }
        }, 1000);
    }

    private void showFailedView(boolean show, String message, @DrawableRes int icon) {
        View lyt_failed = rootView.findViewById(R.id.lyt_failed);
        ((ImageView)rootView.findViewById(R.id.failed_icon)).setImageResource(icon);
        ((TextView) rootView.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) rootView.findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void showNoItemView(boolean show, String message) {
        View lyt_failed = rootView.findViewById(R.id.lyt_failed);
        ((ImageView)rootView.findViewById(R.id.failed_icon)).setVisibility(View.GONE);
        ((TextView) rootView.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) rootView.findViewById(R.id.failed_retry)).setVisibility(View.GONE);
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            shimmer_product.setVisibility(View.GONE);
            shimmer_product.stopShimmer();
            recycler_view.setVisibility(View.VISIBLE);
            return;
        }

        shimmer_product.setVisibility(View.VISIBLE);
        shimmer_product.startShimmer();
        recycler_view.setVisibility(View.INVISIBLE);
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    private void initShimmerLoading() {
        LinearLayout lyt_shimmer_content = rootView.findViewById(R.id.lyt_shimmer_content);
        for (int h = 0; h < 10; h++) {
            LinearLayout row_item = new LinearLayout(myApp);
            row_item.setOrientation(LinearLayout.HORIZONTAL);
            View item = getLayoutInflater().inflate(R.layout.loading_fragment_product_all, null);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 1;
            item.setLayoutParams(p);
            row_item.addView(item);
            lyt_shimmer_content.addView(row_item);
        }
    }
}