package com.tb.tokobersama.ui;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.DrawableRes;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.tb.tokobersama.R;
import com.tb.tokobersama.adapter.AdapterPosition;
import com.tb.tokobersama.model.Downline;
import com.tb.tokobersama.model.Downlines;
import com.tb.tokobersama.model.ResponsePosition;
import com.tb.tokobersama.model.UserProfile;
import com.tb.tokobersama.util.Constants;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;
import com.tb.tokobersama.util.Tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PositionFragment extends Fragment {

    private UserProfile userProfile;

    private ShimmerFrameLayout shimmer_layout;
    private Call<ResponsePosition> callbackCall = null;

    private RecyclerView recycler_view;
    private AdapterPosition mAdapter;

    private View rootView;
    private TextView txt_cur_name, txt_cur_phone, txt_total;
    private ImageView img_up;
    private LinearLayout lyt_title;

    private Downlines application;

    private Context myApp;

    private int count_total = 0;
    private int failed_page = 0;

    private String currentUserPhone, currentUserName, uplineUserPhone, uplineUserName;
    private List<String> uplinePhones = new ArrayList<>();
    private List<String> uplineNames = new ArrayList<>();

    public PositionFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myApp = getActivity();
        application = new Downlines();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_position, container, false);

        initComponent();
        initShimmerLoading();

        List<Downline> downlines = application.getItems();
        int _count_total = application.getCountTotal();
        if (downlines.size() == 0) {
            requestAction(1);
        } else {
            count_total = _count_total;
            displayApiResult(downlines);
        }

        return  rootView;
    }

    private void initComponent() {
        userProfile = Tools.getUserProfile(myApp);

        currentUserPhone = userProfile.getPhone();
        currentUserName = userProfile.getName();

        shimmer_layout = rootView.findViewById(R.id.shimmer_layout);
        recycler_view = rootView.findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(myApp));
        recycler_view.setHasFixedSize(true);
        lyt_title = rootView.findViewById(R.id.lyt_title);

        txt_cur_name = rootView.findViewById(R.id.txt_cur_name);
        txt_cur_phone = rootView.findViewById(R.id.txt_cur_phone);
        img_up = rootView.findViewById(R.id.img_up);
        txt_total = rootView.findViewById(R.id.txt_total);

        //set data and list adapter
        mAdapter = new AdapterPosition(myApp, recycler_view, new ArrayList<Downline>());
        recycler_view.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterPosition.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Downline obj, int position) {
                uplineUserPhone = currentUserPhone;
                uplineUserName = currentUserName;

                currentUserPhone = obj.phone;
                currentUserName = obj.name;

                uplinePhones.add(uplineUserPhone);
                uplineNames.add(uplineUserName);

                mAdapter.resetListData();
                requestAction(1);
            }
        });

        img_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uplinePhones.size()>0) {
                    currentUserPhone = uplinePhones.get(uplinePhones.size()-1);
                    uplinePhones.remove(currentUserPhone);
                }

                if (uplineNames.size()>0) {
                    currentUserName = uplineNames.get(uplineNames.size()-1);
                    uplineNames.remove(currentUserName);
                }
                mAdapter.resetListData();
                requestAction(1);
            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterPosition.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (count_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });
    }

    private void displayApiResult(final List<Downline> items) {
        mAdapter.insertData(items);
        swipeProgress(false);
        setDisplayTitle();
        if (items.size() == 0) showNoItemView(true, "Tidak ada downline");
    }

    private void setDisplayTitle() {
        if (currentUserPhone==userProfile.getPhone())
            img_up.setVisibility(View.GONE);
        else
            img_up.setVisibility(View.VISIBLE);

        txt_cur_name.setText(currentUserName);
        txt_cur_phone.setText(currentUserPhone);
        txt_total.setText("Total downline: "+count_total);
        lyt_title.setVisibility(View.VISIBLE);
    }

    private void requestList(final int page_no) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("page_no",page_no+"");
        parameters.put("item_count", Constants.ITEM_PER_REQUEST +"");
        parameters.put("session",userProfile.getSession());
        //parameters.put("merchant_id",userProfile.getId());
        //parameters.put("phone",userProfile.getPhone());
        parameters.put("phone", currentUserPhone);

        DataService api = RestAdapter.createAPI();

        Call<ResponsePosition> call = api.getPositionList(parameters);

        call.enqueue(new Callback<ResponsePosition>() {
            @Override
            public void onResponse(Call<ResponsePosition> call, Response<ResponsePosition> response) {
                ResponsePosition resp = response.body();
                if (resp!=null && resp.RC.equals("00")) {
                    count_total = Integer.parseInt(resp.total_count);
                    displayApiResult(resp.downline);

                    application.setCountTotal(count_total);
                    application.addItems(resp.downline);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<ResponsePosition> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }
        });
    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (Tools.isConnect(myApp)) {
            showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
        } else {
            showFailedView(true, getString(R.string.no_internet_text), R.drawable.img_no_internet);
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "", R.drawable.img_failed);
        //showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
            application.resetItems();
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestList(page_no);
            }
        }, 1000);
    }

    private void showFailedView(boolean show, String message, @DrawableRes int icon) {
        View lyt_failed = rootView.findViewById(R.id.lyt_failed);
        ((ImageView)rootView.findViewById(R.id.failed_icon)).setImageResource(icon);
        ((TextView) rootView.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) rootView.findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void showNoItemView(boolean show, String message) {
        View lyt_failed = rootView.findViewById(R.id.lyt_failed);
        ((ImageView)rootView.findViewById(R.id.failed_icon)).setVisibility(View.GONE);
        ((TextView) rootView.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) rootView.findViewById(R.id.failed_retry)).setVisibility(View.GONE);
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            //swipe_refresh.setRefreshing(show);
            shimmer_layout.setVisibility(View.GONE);
            shimmer_layout.stopShimmer();
            recycler_view.setVisibility(View.VISIBLE);
            return;
        }

        shimmer_layout.setVisibility(View.VISIBLE);
        shimmer_layout.startShimmer();
        recycler_view.setVisibility(View.INVISIBLE);
        lyt_title.setVisibility(View.INVISIBLE);
//        swipe_refresh.post(new Runnable() {
//            @Override
//            public void run() {
//                swipe_refresh.setRefreshing(show);
//            }
//        });
    }

    private void initShimmerLoading() {
        LinearLayout lyt_shimmer_content = rootView.findViewById(R.id.lyt_shimmer_content);
        for (int h = 0; h < 10; h++) {
            LinearLayout row_item = new LinearLayout(myApp);
            row_item.setOrientation(LinearLayout.HORIZONTAL);
            View item = getLayoutInflater().inflate(R.layout.loading_fragment_transaction, null);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 1;
            item.setLayoutParams(p);
            row_item.addView(item);
            lyt_shimmer_content.addView(row_item);
        }
    }

}