package com.tb.tokobersama.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.DrawableRes;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.tb.tokobersama.AddProductActivity;
import com.tb.tokobersama.ProductDetailActivity;
import com.tb.tokobersama.R;
import com.tb.tokobersama.adapter.AdapterProduct;
import com.tb.tokobersama.model.Product;
import com.tb.tokobersama.model.Products;
import com.tb.tokobersama.model.ResponseProduct;
import com.tb.tokobersama.model.UserProfile;
import com.tb.tokobersama.util.Constants;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;
import com.tb.tokobersama.util.Tools;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductFragment extends Fragment {

    private UserProfile userProfile;

    private SwipeRefreshLayout swipe_refresh;
    private ShimmerFrameLayout shimmer_product;
    private Call<ResponseProduct> callbackCall = null;

    private RecyclerView recycler_view;
    private AdapterProduct mAdapter;

    private View rootView;

    private Products application;

    private Context myApp;

    private int count_total = 0;
    private int failed_page = 0;

    private FloatingActionButton floatingActionButton;

    public ProductFragment() {
        // Required empty public constructor
    }

    public static ProductFragment newInstance(UserProfile userProfile) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putSerializable("UserProfile", userProfile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userProfile = (UserProfile) getArguments().getSerializable("UserProfile");
        }
        //userProfile = ((MainActivity)getActivity()).getUserProfile();
        myApp = getActivity();
        application = new Products();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_product, container, false);

        initComponent();
        initShimmerLoading();

        List<Product> products = application.getProducts();
        int _count_total = application.getCountTotalProduct();
        if (products.size() == 0) {
            requestAction(1);
        } else {
            count_total = _count_total;
            displayApiResult(products);
        }

        return rootView;
    }

    private void initComponent() {
        userProfile = Tools.getUserProfile(myApp); //(MainActivity)getActivity()).getUserProfile();
        shimmer_product = rootView.findViewById(R.id.shimmer_product);
        swipe_refresh = rootView.findViewById(R.id.swipe_refresh);
        recycler_view = rootView.findViewById(R.id.recycler_view);
        floatingActionButton = rootView.findViewById(R.id.fab_prod);
        recycler_view.setLayoutManager(new LinearLayoutManager(myApp));
        recycler_view.setHasFixedSize(true);

        //set data and list adapter
        mAdapter = new AdapterProduct(myApp, recycler_view, new ArrayList<Product>());
        recycler_view.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterProduct.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Product obj, int position) {
                ProductDetailActivity.navigate((Activity)myApp, obj.id);
            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterProduct.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (count_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1);
            }
        });

        if (!userProfile.getStatus().equalsIgnoreCase("active"))
            floatingActionButton.setVisibility(View.GONE);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(myApp, AddProductActivity.class),101);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("Activity Result",requestCode+" "+resultCode);
        if (requestCode==101) {
            if (resultCode==Activity.RESULT_OK) {
                mAdapter.resetListData();
                requestAction(1);
            }
        }
    }

    private void displayApiResult(final List<Product> items) {
        mAdapter.insertData(items);
        swipeProgress(false);
        if (items.size() == 0) showNoItemView(true, "Tidak ada produk");
    }

    private void requestListProductList(final int page_no) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("page_no",page_no+"");
        parameters.put("item_count", Constants.ITEM_PER_REQUEST +"");
        parameters.put("session",userProfile.getSession());
        parameters.put("merchant_id",userProfile.getId());

        DataService api = RestAdapter.createAPI();

        Call<ResponseProduct> call = api.getProductList(parameters);

        call.enqueue(new Callback<ResponseProduct>() {
            @Override
            public void onResponse(Call<ResponseProduct> call, Response<ResponseProduct> response) {
                ResponseProduct resp = response.body();
                if (resp!=null && resp.RC.equals("00")) {
                    count_total = Integer.parseInt(resp.total_count);
                    displayApiResult(resp.products);

                    application.setCountTotalProduct(count_total);
                    application.addProducts(resp.products);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<ResponseProduct> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }
        });
    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (Tools.isConnect(myApp)) {
            showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
        } else {
            showFailedView(true, getString(R.string.no_internet_text), R.drawable.img_no_internet);
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "", R.drawable.img_failed);
        //showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
            application.resetProducts();
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestListProductList(page_no);
            }
        }, 1000);
    }

    private void showFailedView(boolean show, String message, @DrawableRes int icon) {
        View lyt_failed = rootView.findViewById(R.id.lyt_failed);
        ((ImageView)rootView.findViewById(R.id.failed_icon)).setImageResource(icon);
        ((TextView) rootView.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) rootView.findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void showNoItemView(boolean show, String message) {
        View lyt_failed = rootView.findViewById(R.id.lyt_failed);
        ((ImageView)rootView.findViewById(R.id.failed_icon)).setVisibility(View.GONE);
        ((TextView) rootView.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) rootView.findViewById(R.id.failed_retry)).setVisibility(View.GONE);
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            shimmer_product.setVisibility(View.GONE);
            shimmer_product.stopShimmer();
            recycler_view.setVisibility(View.VISIBLE);
            return;
        }

        shimmer_product.setVisibility(View.VISIBLE);
        shimmer_product.startShimmer();
        recycler_view.setVisibility(View.INVISIBLE);
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    private void initShimmerLoading() {
        LinearLayout lyt_shimmer_content = rootView.findViewById(R.id.lyt_shimmer_content);
        for (int h = 0; h < 10; h++) {
            LinearLayout row_item = new LinearLayout(myApp);
            row_item.setOrientation(LinearLayout.HORIZONTAL);
            View item = getLayoutInflater().inflate(R.layout.loading_fragment_product, null);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 1;
            item.setLayoutParams(p);
            row_item.addView(item);
            lyt_shimmer_content.addView(row_item);
        }
    }
}