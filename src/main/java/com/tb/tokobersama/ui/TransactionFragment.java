package com.tb.tokobersama.ui;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.DrawableRes;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.tb.tokobersama.MainActivity;
import com.tb.tokobersama.R;
import com.tb.tokobersama.adapter.AdapterTransaction;
import com.tb.tokobersama.model.ResponseTransaction;
import com.tb.tokobersama.model.Transaction;
import com.tb.tokobersama.model.Transactions;
import com.tb.tokobersama.model.UserProfile;
import com.tb.tokobersama.util.Constants;
import com.tb.tokobersama.util.DataService;
import com.tb.tokobersama.util.RestAdapter;
import com.tb.tokobersama.util.Tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TransactionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TransactionFragment extends Fragment {

    private UserProfile userProfile;

    private SwipeRefreshLayout swipe_refresh;
    private ShimmerFrameLayout shimmer_layout;
    private Call<ResponseTransaction> callbackCall = null;

    private RecyclerView recycler_view;
    private AdapterTransaction mAdapter;

    private View rootView;

    private Transactions application;

    private Context myApp;

    private int count_total = 0;
    private int failed_page = 0;

    public TransactionFragment() {
        // Required empty public constructor
    }

    public static TransactionFragment newInstance(UserProfile userProfile) {
        TransactionFragment fragment = new TransactionFragment();
        Bundle args = new Bundle();
        args.putSerializable("UserProfile",userProfile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userProfile = (UserProfile) getArguments().getSerializable("UserProfile");
        }
        userProfile = ((MainActivity)getActivity()).getUserProfile();
        myApp = getActivity();
        application = new Transactions();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_transaction, container, false);

        initComponent();
        initShimmerLoading();

        List<Transaction> transactions = application.getItems();
        int _count_total = application.getCountTotal();
        if (transactions.size() == 0) {
            requestAction(1);
        } else {
            count_total = _count_total;
            displayApiResult(transactions);
        }

        return rootView;
    }

    private void initComponent() {
        shimmer_layout = rootView.findViewById(R.id.shimmer_layout);
        swipe_refresh = rootView.findViewById(R.id.swipe_refresh);
        recycler_view = rootView.findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new LinearLayoutManager(myApp));
        recycler_view.setHasFixedSize(true);

        //set data and list adapter
        mAdapter = new AdapterTransaction(myApp, recycler_view, new ArrayList<Transaction>());
        recycler_view.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new AdapterTransaction.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Transaction obj, int position) {
                //ActivityVideoDetails.navigate(myApp, obj.id, false);
            }
        });

        // detect when scroll reach bottom
        mAdapter.setOnLoadMoreListener(new AdapterTransaction.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (count_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1);
            }
        });
    }

    private void displayApiResult(final List<Transaction> items) {
        mAdapter.insertData(items);
        swipeProgress(false);
        if (items.size() == 0) showNoItemView(true, "Tidak ada transaksi");
    }

    private void requestList(final int page_no) {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("page_no",page_no+"");
        parameters.put("item_count", Constants.ITEM_PER_REQUEST +"");
        parameters.put("session",userProfile.getSession());
        parameters.put("merchant_id",userProfile.getId());

        DataService api = RestAdapter.createAPI();

        Call<ResponseTransaction> call = api.getTransactionList(parameters);

        call.enqueue(new Callback<ResponseTransaction>() {
            @Override
            public void onResponse(Call<ResponseTransaction> call, Response<ResponseTransaction> response) {
                ResponseTransaction resp = response.body();
                if (resp!=null && resp.RC.equals("00")) {
                    count_total = Integer.parseInt(resp.total_count);
                    displayApiResult(resp.transactions);

                    application.setCountTotal(count_total);
                    application.addItems(resp.transactions);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<ResponseTransaction> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }
        });
    }

    private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (Tools.isConnect(myApp)) {
            showFailedView(true, getString(R.string.failed_text), R.drawable.img_failed);
        } else {
            showFailedView(true, getString(R.string.no_internet_text), R.drawable.img_no_internet);
        }
    }

    private void requestAction(final int page_no) {
        showFailedView(false, "", R.drawable.img_failed);
        //showNoItemView(false);
        if (page_no == 1) {
            swipeProgress(true);
            application.resetItems();
        } else {
            mAdapter.setLoading();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestList(page_no);
            }
        }, 1000);
    }

    private void showFailedView(boolean show, String message, @DrawableRes int icon) {
        View lyt_failed = rootView.findViewById(R.id.lyt_failed);
        ((ImageView)rootView.findViewById(R.id.failed_icon)).setImageResource(icon);
        ((TextView) rootView.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) rootView.findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });
    }

    private void showNoItemView(boolean show, String message) {
        View lyt_failed = rootView.findViewById(R.id.lyt_failed);
        ((ImageView)rootView.findViewById(R.id.failed_icon)).setVisibility(View.GONE);
        ((TextView) rootView.findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recycler_view.setVisibility(View.INVISIBLE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recycler_view.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) rootView.findViewById(R.id.failed_retry)).setVisibility(View.GONE);
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            shimmer_layout.setVisibility(View.GONE);
            shimmer_layout.stopShimmer();
            recycler_view.setVisibility(View.VISIBLE);
            return;
        }

        shimmer_layout.setVisibility(View.VISIBLE);
        shimmer_layout.startShimmer();
        recycler_view.setVisibility(View.INVISIBLE);
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    private void initShimmerLoading() {
        LinearLayout lyt_shimmer_content = rootView.findViewById(R.id.lyt_shimmer_content);
        for (int h = 0; h < 10; h++) {
            LinearLayout row_item = new LinearLayout(myApp);
            row_item.setOrientation(LinearLayout.HORIZONTAL);
            View item = getLayoutInflater().inflate(R.layout.loading_fragment_transaction, null);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 1;
            item.setLayoutParams(p);
            row_item.addView(item);
            lyt_shimmer_content.addView(row_item);
        }
    }


}