package com.tb.tokobersama.util;

public class Constants {

    public static String API_BASE_URL = "https://www.tokobersama.co.id/comm/api/";
    public static String key_string = "tokobersama";
    public static int ITEM_PER_REQUEST = 20;
    public static String MY_PREFS_NAME = "toko_bersama";
}
