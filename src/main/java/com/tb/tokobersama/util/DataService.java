package com.tb.tokobersama.util;

import com.tb.tokobersama.model.ResponseCategory;
import com.tb.tokobersama.model.ResponseMerchant;
import com.tb.tokobersama.model.ResponseNotification;
import com.tb.tokobersama.model.ResponseOTP;
import com.tb.tokobersama.model.ResponsePosition;
import com.tb.tokobersama.model.ResponseProduct;
import com.tb.tokobersama.model.ResponseRegister;
import com.tb.tokobersama.model.ResponseService;
import com.tb.tokobersama.model.ResponseToken;
import com.tb.tokobersama.model.ResponseTopup;
import com.tb.tokobersama.model.ResponseTransaction;
import com.tb.tokobersama.model.ResponseUpload;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;

public interface DataService {

    @FormUrlEncoded
    @POST("login")
    Call<String> login(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("otp")
    Call<ResponseOTP> getOTP(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("register")
    Call<ResponseRegister> register(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("product")
    Call<ResponseProduct> getProductList(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("transaction")
    Call<ResponseTransaction> getTransactionList(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("notification")
    Call<ResponseNotification> getNotificationList(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("disburse")
    Call<ResponseToken> requestDisbursement(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("service")
    Call<ResponseService> getService(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("topup")
    Call<ResponseTopup> requestTopup(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("position")
    Call<ResponsePosition> getPositionList(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("fcm")
    Call<ResponseToken> uploadToken(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("deviceid")
    Call<ResponseToken> setDeviceId(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("merchant")
    Call<ResponseMerchant> getMerchantInfo(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("category")
    Call<ResponseCategory> getCategoryList(@FieldMap HashMap<String, String> params);

    @Multipart
    @POST("product_add")
    Call<ResponseUpload> uploadProduct(@PartMap Map<String, RequestBody> texts,
                                       @Part List<MultipartBody.Part> files);

}
