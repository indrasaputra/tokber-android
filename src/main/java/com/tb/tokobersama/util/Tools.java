package com.tb.tokobersama.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.tb.tokobersama.R;
import com.tb.tokobersama.model.UserProfile;
import com.squareup.picasso.Picasso;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class Tools {

    public static void displayImage(Context ctx, ImageView img, String url) {
        Log.d("image url ===", url);
        AlphaAnimation anim = new AlphaAnimation(0, 1);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.ABSOLUTE);
        anim.setDuration(1000);
        if (url.equals("") || url.equals(null)) {
            Picasso.with(ctx).load(R.drawable.icon_tb).into(img);
        } else {
            Picasso.with(ctx).load(url).into(img);
        }
        img.startAnimation(anim);
    }

    public static boolean isConnect(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                if (activeNetworkInfo.isConnected() || activeNetworkInfo.isConnectedOrConnecting()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e){
            return false;
        }
    }

    public static String formatDateTime(String oldDateTime) {
        String newDateTime = "";
        final String OLD_FORMAT = "yyyyMMddHHmmss";
        final String NEW_FORMAT = "dd MMM yyyy HH:mm";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
            Date d = sdf.parse(oldDateTime);
            sdf.applyPattern(NEW_FORMAT);
            newDateTime = sdf.format(d);
        } catch (ParseException e) {
            Log.e("Error date",e.getMessage());
        }

        return newDateTime;
    }

    public static boolean checkAndRequestPermissions(Context context) {


        int permissionSendMessage = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        int locationPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity)context, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),1);
            return false;
        }
        return true;
    }

    public static UserProfile getUserProfile(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.MY_PREFS_NAME, Context.MODE_PRIVATE);

        UserProfile userProfile = new UserProfile();

        userProfile.setId(prefs.getString("user_id",null));
        userProfile.setPhone(prefs.getString("user_phone",null));
        userProfile.setName(prefs.getString("user_name",null));
        userProfile.setAddress(prefs.getString("user_address",null));
        userProfile.setProvince(prefs.getString("user_province",null));
        userProfile.setCity(prefs.getString("user_city",null));//user_city,
        userProfile.setEmail(prefs.getString("user_email",null));//user_email,
        userProfile.setStore_name(prefs.getString("user_store_name",null));//user_store_name,
        userProfile.setStore_description(prefs.getString("user_store_description",null));//user_store_description,
        userProfile.setBalance(prefs.getString("user_balance",null));//user_balance,
        userProfile.setReg_date(prefs.getString("user_reg_date",null));//user_reg_date,
        userProfile.setUpline(prefs.getString("user_upline",null));//user_upline,
        userProfile.setLon(prefs.getString("user_lon",null));//user_lon,
        userProfile.setLat(prefs.getString("user_lat",null));//user_lat,
        userProfile.setBank_name(prefs.getString("user_bank_name",null));//user_bank_name,
        userProfile.setAccount_name(prefs.getString("user_bank_account_name",null));//user_bank_account_name,
        userProfile.setBank_account_number(prefs.getString("user_bank_account_number",null));//user_bank_account_number,
        userProfile.setPostal_code(prefs.getString("user_postal_code",null));//user_postal_code,
        userProfile.setStatus(prefs.getString("user_status",null));//user_status,
        userProfile.setSession(prefs.getString("user_session",null));//user_session;
        userProfile.setPin(prefs.getString("user_pin",null));//user_pin;
        userProfile.setNotif(prefs.getString("user_notif",null));//user_pin;

        return userProfile;
    }

    public static String getTrxid(String merchant_id) {
        String trxid = "";

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder builder = new StringBuilder();
        builder.append("tb")
                .append(merchant_id)
                .append(timestamp);
        int length = 5;
        while (length-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }

        trxid = builder.toString().toUpperCase();

        return trxid;
    }
}
